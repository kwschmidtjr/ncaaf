# Pull base image
FROM python:3.8.0

# Set work directory
RUN mkdir /code
WORKDIR /code

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install dependencies
COPY requirements.txt /code/
RUN python3 -m pip install -r requirements.txt --no-cache-dir

# Copy project
COPY src /code/
