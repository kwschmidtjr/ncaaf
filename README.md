Overview
========

This is the source code for CollegeFBPool.com.  There are three primary applications:

1) *apps.pool*.  This is the application that contains the core logic of the Web site.
Most of this logic is in *apps.pool.models*.

2) *apps.stats*.  This is an interface for getting matchup and game stats from an external
API.  The current implementation is via ESPN, but that may change in the future.

3) *apps.member*.  This has an implementation of the user Profile.  This also contains an
SMS object that provides an interface for queueing and sending text messages.

A good view of the object model is here: http://www.collegefbpool.com/static/img/object_model.png

CI/CD
=====

Various Files
-------------

- `Makefile` - this describes various tasks that can be used during local
  development as well as on the CI server during a Gitlab build pipeline
  - the goal was for all tasks to be able to be run from either a local
    developer machine or on a CI server

- `Dockerfile` - this describes how to build the Django application Docker
  image
  - this same image is also used to run both the `celery` and `celery-beat`
    services because it contains all the code needed to run all three; the only
    difference when using the image is the Compose `command` used

- `Dockerfile.nginx` - this describes how to build the custom nginx Docker image
  that contains the production nginx config and all of the static files served
  by the Django application

- `docker-compose.local.yml` - this Compose file describes a full cluster
  meant to be run on a developer machine (with the exception of nginx)

- `docker-compose.prod.yml` - the Compose file that describes the
  prod cluster deployed to GCP via Gitlab
  - `nginx` service - this Compose service contains an nginx configuration that is
    setup to work with certbot for SSL termination; this service relies on two
    [bind-mount](https://docs.docker.com/storage/bind-mounts/) Docker volumes
    that map directories on the GCP VM host into the container:
    - `/etc/letsencrypt` - this folder contains the actual SSL certificates that
      are issued/renewed by certbot via Let's Encrypt
    - `/var/www/certbot` - this directory maps to the `/.well-known/acme-challenge`
      URL in the nginx config; certbot needs to place files here during a SSL
      certificate renewal so that Let's Encrypt can verify ownership of the
      `collegefbpool.com` domain
  - `celery` / `celery-beat` services - these use the same image as the `web` service

- `requirements.txt` - used to install the required Python dependencies for the
  Django application (both locally for development and within `Dockerfile`)

### Archived Files

- `archive/init-letsencrypt.sh` - this was the original script used to setup
  certbot and Let's Encrypt on the GCP VM

- `archive/nginx.conf` - this was an nginx configuration file that was
  previously used to run nginx via Docker Compose on a developer machine;
  the nginx service was removed from the local Compose file to simplify
  all of the configuration files needing to be managed;
  trying to manage a local nginx config *without* certbot and a prod config *with*
  certbot is not the easiest, and one can use the built-in Django webserver
  when doing developer testing without the complexity of nginx


Gitlab & Gitlab Runner
----------------------

Originally a self-managed Gitlab Runner was installed on the prod GCP VM to be
used to run all of the CI/CD pipeline stages.  It would run the tests, build and
push containers for the Django app and nginx, and also to deploy the application to Prod.

This setup has been modified to try to push most of the build stages onto
[SaaS Runners] (https://docs.gitlab.com/ee/ci/runners/) which are managed by
Gitlab.com, which is a more conventional setup.

The deployment stage is still being run via the self-managed Runner because it
allows access to the prod GCP VM to run `docker-compose` commands that will
deploy the application.  This is something that still could be moved onto a SaaS
Runner at a future date by doing something like SSH'ing from a SaaS Runner to the GCP VM
to run the `docker-compose` commands.

The build stage is still being run on the self-managed Runner because there
wasn't enough time on the last contract to make all the changes needed to move
that stage to a SaaS Runner (things like finding a new build stage `image` that
has `make` and `docker` installed on it).

### Gitlab Runner Tags

The `tags` sections of the pipeline jobs defined in `.gitlab-ci.yml` are what map a
job to a specific Gitlab Runner.

Currently, the
[self-managed Gitlab Runner](https://gitlab.com/kwschmidtjr/ncaaf/-/runners/2642627)
that is installed on the prod GCP VM is assigned the tags: `prod` and `shell`.
Therefore, any pipeline jobs tagged with `prod` and `shell` will be run on that
self-managed Runner.

All of the other pipleine stages will be run on gitlab.com's Saas Runners, which
is the default behavior.

Gitlab Registry
---------------

The Gitlab builds will push the Django app and custom nginx Docker images to the
Gitlab Registry for this repository at:

- https://gitlab.com/kwschmidtjr/ncaaf/container_registry

There are several images in that Registry but the following two are the
ones currently still being used:

  - `ncaaf/app` - Django app image
  - `ncaaf/nginx` - custom nginx image used in prod
    - this image was previously used to store the "local" custom nginx image when
      there was both a local and prod nginx image; now there is only one image
      since the local Docker Compose file no longer uses nginx

The following are legacy images:

  - `ncaaf` - the original Django app image
  - `ncaaf/nginx-prod` - the original custom nginx image used for prod when the
    project had two different nginx images (one for dev; one for prod)


The reason for sticking with the name `nginx` vs. `nginx-prod` for the nginx
image is because we typically don't want to have to create a new image for each
environment.  Otherwise we run into image bloat (dev, staging, prod, etc.) and
management of all the images becomes difficult.

It is better practice to try to create a single image that can be configured
per-environment by things like environment variables.  This way we can localize all
environment-specific values to just config files.

Note: the current `ncaaf/nginx` image DOES contain prod-specific config values.
However, there is currently only one environment where nginx is used (prod), and
there was not time on this last contract to extract those values.
When it comes time to need another environment, that would be the best time to
make the image more generic (see more notes about this in the Recommendations
section below).

Certbot / Let's Encrypt
=======================

Certbot was originally running inside a Docker container alongside nginx.  In
8/2023, this setup was changed so that certbot is running natively on the prod
GCP VM versus inside of a container.  This was done by installing certbot via
the Snap package manager.

This was done for several reasons:

- using the native certbot gives a Systemd cron (technically termed a "timer")
  that will periodically run to automatically renew the SSL certificates
  - using the Systemd timer eliminates the need to have to roll your own cron mechanism
- running certbot inside Docker is not recommended by certbot

The tradeoff of this setup is that:

- we need to bind-mount host directories into the `nginx` Compose container
  which makes the current deployment setup a less portable
- we need to run the initial cert issuance on the prod VM manually before the nginx
  container is ever run on the server - however this needs to be done only once
  (this manual step could also be automated with something like Ansible)

Detailed certbot Setup
----------------------

These are the steps that were taken to setup the certbot SSL certificates and
the renewal process:

- The `cerbot` Snap package was installed on the prod GCP VM which installs a
  `certbot` daemon and Systemd renewal cron job.
- Initial SSL certificates were issued with certbot manually via the `standalone` method on
  the prod GCP VM when the nginx container was NOT running
  - the nginx container could not be running because certbot needs to bind to
    port 80 during this `standalone` issuance step
- The nginx container is deployed to the prod server and configured to mount the host directory
  `/var/www/certbot` into the running container.
- A `certbot reconfigure` command was run that sets up certbot to do all future,
  automated renewals using the `webroot` method with `/var/www/certbot` as the webroot directory.
  - this method will allow the running nginx container to serve up the HTTP
    validation files required for renewal at the `/.well-known/acme-challenge` URL
  - the validation files will be placed in `/var/www/certbot` by the certbot daemon running
    on the host OS

Going forward, the `certbot` Systemd timer will periodically renew the SSL certificates.


### Helper Scripts

Some helper scripts that show the steps that were run to install certbot, issue the
initial certs and setup the renewal are included in the `scripts/` directory of
this repo.


Future Recommendations
======================

- try to extract some of the hard-coded, and environment-specific, values in the
  nginx configuration file, like the DNS name (`collegefbpool.com`) in the
  `server` block, into environment variables or by using nginx templates
  (the public nginx Docker image allows for templating)
- extract the secret values from the various Django `settings/*.py` files into
  environment variables
  - you can then use Gitlab's CI/CD Variables to store the secrets as maske
    values along with Docker Compose's ability to use environment variables to
    perform deployments to `prod`
- develop a Git tagging strategy that you can then use to tag Docker
  images so that you can get images with versions like `v0.1.0` or `1.0.0` vs. git
  commit SHA's like `04adee7b5d459fe2337d6a342c6bd272874d9680`.
- look into the current `requirements.txt` and resolve any outstanding dependency issues/warnings
- move the build and deploy stages of the CI/CD pipeline in `.gitlab-ci.yml` to
  SaaS Runners


Terminology
===========

### Environments

- `local` - this environment represents the services running on a developer's
  machine; `dev` was not chosen to represent this because a `dev` environment
  would typically refer to a test instance of the app but deployed on a remote
  host (like a GCP VM)

- `prod` - this environment is the production environment where the application
  is deployed in GCP
