"""ncaaf URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.views import LoginView
from django.views.generic import TemplateView
from django.urls import path, include
from .apps.pool.views import home

urlpatterns = [
    path('admin/', admin.site.urls),
    path('pool/', include('ncaaf.apps.pool.urls')),
    path('member/', include('ncaaf.apps.member.urls')),
    path('stats/', include('ncaaf.apps.stats.urls')),
    path('', home, name='home'),
    url(r'member/login/$', LoginView.as_view(template_name='member/login.html'), name="login"),
    url(r'about', TemplateView.as_view(template_name="pool/about.html"), name='about'),
    # url(r'member/logout/$', LogoutView.as_view(), name="logout"),
    url(r'member/logout/$', home, name='home'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

