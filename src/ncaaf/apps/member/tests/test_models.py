from django.test import TestCase
from ..models import SMS


class SMSQueue(TestCase):

    def test_empty_queue(self):
        queue = SMS.get_messages("7045551212")
        self.assertEqual(len(queue), 0)

    def test_one_queue(self):
        phone = "7045551212"
        message = "message"

        SMS.queue_message(message, phone)
        SMS.queue_message(message, phone)
        queue = SMS.get_messages(phone)

        self.assertEqual(len(queue), 2)

    def test_queue_message_with_tag(self):
        phone = "7045551212"
        message = "message"
        espn_id = "1234567"

        tag = phone + "|" + espn_id

        SMS.queue_message(message, phone, tag)
        SMS.send_queued_messages(True)
        SMS.queue_message(message, phone, tag)
        queue = SMS.get_messages(phone)

        self.assertEqual(len(queue), 0)

    def test_queue_two_messages_with_tag(self):
        phone1 = "7045551212"
        phone2 = "8645551212"
        message = "message"
        espn_id = "1234567"

        tag1 = phone1 + "|" + espn_id
        tag2 = phone2 + "|" + espn_id

        SMS.queue_message(message, phone1, tag1)
        SMS.queue_message(message, phone2, tag2)
        SMS.send_queued_messages(True)
        SMS.queue_message(message, phone1, tag1)
        SMS.queue_message(message, phone2, tag2)

        queue = SMS.get_messages(phone1)
        self.assertEqual(len(queue), 0)

        queue = SMS.get_messages(phone2)
        self.assertEqual(len(queue), 0)

    def test_queue_message(self):
        phone1 = "7045551212"
        phone2 = "8645551212"
        message1 = "7056 message 1"
        message2 = "9557 message 1"

        SMS.queue_message(message1, phone1)
        SMS.queue_message("7056 message 2", phone1)
        SMS.queue_message(message2, phone2)

        queue = SMS.get_messages(phone1)
        self.assertEqual(queue[0], message1)

        queue = SMS.get_messages(phone2)
        self.assertEqual(queue[0], message2)

    def test_queue_message_to_all(self):
        phone1 = "7045551212"
        phone2 = "8645551212"
        msg_to_both ="message"

        SMS.queue_message("p1 m1", phone1)
        SMS.queue_message("p2 m1", phone2)
        SMS.queue_message_to_all(msg_to_both)

        queue = SMS.get_messages(phone1)
        self.assertEqual(len(queue), 2)
        self.assertEqual(queue[1], msg_to_both)

        queue = SMS.get_messages(phone2)
        self.assertEqual(len(queue), 2)
        self.assertEqual(queue[1], msg_to_both)

    def test_queue_message_to_all_with_sent(self):
        phone1 = "7045551212"
        phone2 = "8645551212"
        message ="message"

        SMS.queue_message("p1 m1", phone1)
        SMS.queue_message("p2 m1", phone2)
        SMS.queue_message_to_all(message)
        SMS.send_queued_messages(True)

        SMS.queue_message("p1 m1", phone1)
        SMS.queue_message_to_all(message)

        queue = SMS.get_messages(phone1)
        self.assertEqual(len(queue), 2)

        queue = SMS.get_messages(phone2)
        self.assertEqual(len(queue), 0)

    def test_send_queued_messages(self):
        phone1 = "7045551212"
        phone2 = "8645551212"
        phone3 = "4045551212"

        SMS.queue_message("message", phone1)
        SMS.queue_message("message", phone2)
        SMS.queue_message("message", phone3)

        SMS.send_queued_messages(dry_run=True)

        queue = SMS.get_messages(phone1)
        self.assertEqual(len(queue), 0)

        queue = SMS.get_messages(phone2)
        self.assertEqual(len(queue), 0)

        queue = SMS.get_messages(phone3)
        self.assertEqual(len(queue), 0)
