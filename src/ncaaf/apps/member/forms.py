from django import forms
from .models import Profile


class MyImageInput(forms.FileInput):
    template_name = 'widgets/profile_image.html'


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['short_name', 'image', 'mobile_phone', 'text_reminder', 'auto_pick']

    short_name = forms.CharField(label='', max_length=4, widget=forms.TextInput(attrs={'size': '4'}))
    image = forms.ImageField(label='', widget=MyImageInput(), required=False)
    mobile_phone = forms.CharField(label='', max_length=17, widget=forms.TextInput(attrs={'size': '17'}))
    text_reminder = forms.BooleanField(
        label='Receive a text reminder three hours prior to kickoff to make your pick',
        required=False
    )
    auto_pick = forms.BooleanField(
        label='If your pick is still not in, make a random pick at the cutoff time',
        required=False
    )
