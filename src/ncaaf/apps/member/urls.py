from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from .views import profile, password

urlpatterns = [
    url(r'profile/', profile, name='profile'),

    url(r'^password_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(template_name='member/password_reset_confirm.html'),
        name='password_reset_confirm'),

    url(r'^password_reset_done/',
        auth_views.PasswordResetDoneView.as_view(
            template_name='member/password_reset_done.html'),
        name='password_reset_done'),

    url(r'^password_reset_complete/',
        auth_views.PasswordResetCompleteView.as_view(template_name='member/password_reset_complete.html'),
        name='password_reset_complete'),

    url(r'^password_reset/',
        auth_views.PasswordResetView.as_view(
            template_name='member/password_reset_form.html',
            subject_template_name='member/password_reset_subject.txt',
            email_template_name='member/password_reset_email.html'),
        name='password_reset'),

    url(r'^password/$', password, name='password'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
