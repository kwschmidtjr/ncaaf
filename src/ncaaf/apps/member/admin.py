from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from ..member.models import User, Profile, SMS


class NcaafUserAdmin(ImportExportModelAdmin):
    list_display = ('username', 'email', 'name', 'last_login', 'commish')

    def name(self, obj):
        return obj.get_short_name()

    def commish(self, obj):
        return obj.is_commissioner


admin.site.register(User, NcaafUserAdmin)


class ProfileAdmin(ImportExportModelAdmin):
    list_display = ('name', 'short_name', 'mobile', 'text_reminder', 'auto_pick', 'commish')

    def commish(self, obj):
        return obj.is_commissioner

    def mobile(self, obj):
        num = obj.mobile_phone
        return num[:3]+'-'+num[3:6]+'-'+num[6:]


admin.site.register(Profile, ProfileAdmin)


class SMSAdmin(ImportExportModelAdmin):
    list_display = ('id', 'sent', 'recipient', 'tag', 'message')


admin.site.register(SMS, SMSAdmin)
