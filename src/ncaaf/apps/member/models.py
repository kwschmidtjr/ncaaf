"""Provides a user profile model as well as an object for queueing and sending SMS messages."""
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.conf import settings
from django.core.validators import RegexValidator
from twilio.rest import Client


class User(AbstractUser):

    class Meta:
        ordering = ['id']

    is_commissioner = models.BooleanField(default=False)


phone_regex = RegexValidator(
    regex=r'^\+?1?\d{9,15}$',
    message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."
)


def profile_pic_filename(instance, filename):
    """Returns a filename in the layout 'profile_pics/user_<id>.<filename extension>'

    For example:  profile_pics/user_1.jpg
    """
    file_extension = filename.split('.')[-1]
    upload_filename = 'profile_pics/user_{}.{}'.format(instance.user.id, file_extension)
    return upload_filename


class Profile(models.Model):

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    short_name = models.CharField(default='', max_length=4)
    image = models.ImageField(default='profile_pics/default-profile-pic.png', upload_to=profile_pic_filename)
    mobile_phone = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    text_reminder = models.BooleanField(default=False)
    auto_pick = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.user.first_name} Profile'

    def name(self):
        return self.user.first_name

    def is_commissioner(self):
        return self.user.is_commissioner


class SMS(models.Model):
    """Provides an interface for queueing and sending SMS messages."""

    # For reference: https://www.twilio.com/docs/sms/quickstart/python#send-an-outbound-sms-with-python

    message = models.TextField(default='')
    recipient = models.CharField(max_length=15, default='')
    tag = models.CharField(max_length=30, default='')
    sent = models.BooleanField(default=False)

    @staticmethod
    def queue_message(message, recipient, tag=''):
        """Adds an SMS text message to the queue for the specified recipient.

        :param str message: the text of the message to send
        :param str recipient: the phone number of the recipient in the format 'NNNNNNNNNN'.
            For example, '7047377056'.  Formatting is not checked but the message will fail to send
            if this format is not followed.
        :param str tag: Optional.  A tag is used to ensure that a message is sent only once.  If a subsequent
            attempt is made to queue a message with a tag, the message will not be added to the queue.
        :return: bool: True if message queued, and False if not
        """
        if len(tag) > 0:
            message_list = SMS.objects.filter(tag=tag, sent=True)
            if len(message_list) > 0:
                return False

        SMS.objects.create(message=message, recipient=recipient, tag=tag, sent=False)
        return True

    @staticmethod
    def queue_message_to_all(message):
        """Adds an SMS text message to the queue for each recipient that has unsent messages in the queue.

        The tag for each message will be empty (e.g. '').

        :param str message: the text of the message to send
        :return: nothing
        """
        if settings.DATABASES['default']['ENGINE'] == 'django.db.backends.sqlite3':
            pass
        # recipient_list = SMS.objects.filter(sent=False).distinct('recipient')
        recipient_list = SMS.objects.filter(sent=False).values('recipient').distinct()
        for recipient in recipient_list:
            SMS.objects.create(recipient=recipient['recipient'], message=message, tag='', sent=False)

    @staticmethod
    def get_messages(recipient):
        """Returns the list of unsent messages for the specified recipient

        :param str recipient: the phone number of the recipient in the format 'NNNNNNNNNN'.
            For example, '7047377056'.
        :return: list of strings.  Empty list if there are no messages for the specified recipient
        """
        message_list = SMS.objects.filter(recipient=recipient, sent=False)
        if len(message_list) == 0:
            return []

        messages = []
        for message in message_list:
            messages.append(message.message)
        return messages

    @staticmethod
    def send_queued_messages(dry_run=False):
        """Sends all messages in the queue to each recipient in the queue.

        For each unique recipient, all the messages in the queue are combined into a single message in
        the order they were queued.  A line break will be inserted between each message.  For example,
        if the queue looks like this (note that | represents field divider):
            recipient   |  message   |  tag    |  sent\n
            5555551212  |  hi        |  abc    |  False\n
            5555551212  |  how       |  def    |  False\n
            5555551212  |  are       |  ghi    |  False\n
            5555551212  |  you       |  jkl    |  False\n
        The message sent will look like this:
            hi\n
            how\n
            are\n
            you\n
        After sending the messages each will be marked as sent

        :param bool dry_run: if True, the actual SMS message will not be sent.  All the other logic will run.
            This is useful for testing all the logic but not initiating a bunch of SMS messages.
        :return: int number of notifications sent
        """
        notifications_sent = 0
        # recipient_list = SMS.objects.filter(sent=False).distinct('recipient')
        recipient_list = SMS.objects.filter(sent=False).values('recipient').distinct()

        # TODO: this implementation depends on send_queued_messages being only invoked
        # by a single source.  The real way to implement this is to have all this in a single
        # database transaction.  It's not critical for the current implementation so I didn't
        # bother.
        for recipient in recipient_list:
            msg = ""
            message_list = SMS.objects.filter(sent=False, recipient=recipient['recipient'])
            for message in message_list:
                msg += message.message + "\n"
            if not dry_run:
                SMS.send_message(msg, recipient['recipient'])
            notifications_sent += 1
            SMS.objects.filter(sent=False, recipient=recipient['recipient']).update(sent=True)
        return notifications_sent

    @staticmethod
    def send_message(message, recipient):
        """Sends an SMS text message immediately to the specified recipient without putting it on the message queue.

        In order to queue a message for sending in the future, use queue_message.

        :param str message: the text of the message to send
        :param str recipient: the phone number of the recipient in the format 'NNNNNNNNNN'.
            For example, '7047377056'.
        :return: nothing
        """
        # https://www.twilio.com/docs/libraries/python
        client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
        message = client.messages.create(
            body=message,
            from_=settings.TWILIO_FROM,
            to='+1'+recipient)
        # print('To: {} | Message: {}'.format(recipient, message))

    @staticmethod
    def send_message_to_all(message):
        """Sends an SMS text message to every user in the application

        :param str message: the text of the message to send
        :return: nothing
        """
        profile_list = Profile.objects.all()
        for profile in profile_list:
            # if profile.user.username in ['kwschmidtjr', 'jeananne']
            if profile.user.username == 'kwschmidtjr':
                SMS.send_message(message, profile.mobile_phone)
