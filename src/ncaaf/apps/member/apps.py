from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'ncaaf.apps.member'
    verbose_name = 'NCAAF Users'
