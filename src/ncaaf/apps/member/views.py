from django.shortcuts import render
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages
from django.shortcuts import render, redirect
from .forms import ProfileForm
from .models import Profile


@login_required()
def profile(request):

    template_name = 'member/profile.html'
    title = 'CFP - Profile'

    # user_profile = Profile.objects.get_or_create(user=request.user)
    try:
        user_profile = Profile.objects.get(user=request.user)
    except Profile.DoesNotExist:
        # This is the first time this user creates a profile
        user_profile = Profile.objects.create(user=request.user)

    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=user_profile)
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully saved")
    else:
        form = ProfileForm(instance=user_profile)

    return render(request, template_name, {'form': form, 'title': title})


@login_required()
def password(request):

    template_name = 'member/password.html'
    title = 'CFP - Profile - Password'

    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, template_name, {'form': form, 'title': title})
