# from datetime import datetime
from django.contrib.auth import get_user_model
from django import forms
from django.forms import modelformset_factory, BaseModelFormSet
# import pytz
# from bootstrap_datepicker_plus import DateTimePickerInput
from .models import CONFERENCE_CHOICES, WEEK_CHOICES, YEAR_CHOICES, Matchup, Pick, Team, Config, TZ


class TeamsForm(forms.Form):
    conference = forms.CharField(label="",
                                 widget=forms.Select(
                                     choices=CONFERENCE_CHOICES,
                                     attrs={"onChange": 'this.form.submit()'})
                                 )


class TeamModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return "%s" % obj.code


class MatchupForm(forms.ModelForm):
    """The MatchupForm is used by the pool administrator to select games for the week."""
    favorite_choice = forms.CharField(label='', required=False)
    away_code = forms.CharField(required=False)
    home_code = forms.CharField(required=False)
    away_espn_id = forms.CharField(required=False)
    home_espn_id = forms.CharField(required=False)
    state = forms.CharField(label='', widget=forms.Select(choices=Matchup.MATCHUP_STATE))
    opening_line = forms.FloatField(
        widget=forms.NumberInput(attrs={'size': '3', 'step': '0.5'}), label='', min_value=0, required=True, initial=0)
    pick_count = forms.IntegerField(required=False)
    game_info = forms.CharField(required=False)

    class Meta:
        model = Matchup
        fields = ['opening_line', 'state', 'game_info']

    def __init__(self, *args, **kwargs):
        super(MatchupForm, self).__init__(*args, **kwargs)

    def clean(self):
        favorite_choice = self.cleaned_data['favorite_choice']
        state = self.cleaned_data['state']
        if favorite_choice == '':
            if state == Matchup.PICKS:
                matchup = self.cleaned_data['id']
                message = 'Matchup needs a favorite: {}'.format(matchup)
                raise forms.ValidationError(message)

    def save(self, commit=True, *args, **kwargs):
        matchup = super(MatchupForm, self).save(commit=False, *args, **kwargs)
        favorite_choice = self.cleaned_data['favorite_choice']
        matchup.favorite = Team.objects.filter(code=favorite_choice).first()
        if commit:
            matchup.save()


class BaseMatchupFormSet(BaseModelFormSet):

    def clean(self):
        for form in self.forms:
            form.clean()


MatchupFormSet = modelformset_factory(Matchup, form=MatchupForm, formset=BaseMatchupFormSet, extra=0)
MatchupFormSetPlusOne = modelformset_factory(Matchup, form=MatchupForm, formset=BaseMatchupFormSet, extra=1)


class TeamDisplayField(forms.ModelChoiceField):
    widget = forms.HiddenInput()

    def label_from_instance(self, obj):
        return "%s" % obj.code


class ScoreForm(forms.ModelForm):
    """The ScoreForm is used by the pool administrator to enter scores for each matchup of the week."""
    away_code = forms.CharField(required=False)
    home_code = forms.CharField(required=False)
    away_espn_id = forms.CharField(required=False)
    home_espn_id = forms.CharField(required=False)
    home_score = forms.IntegerField(min_value=0, label='')
    away_score = forms.IntegerField(min_value=0, label='')

    class Meta:
        model = Matchup
        fields = ['away_team', 'home_team', 'home_score', 'away_score']
        exclude = ['year', 'week', 'favorite', 'opening_line', 'date_time_of_game']
        # TODO: use this same technique on the PicksForm to avoid specifying a queryset in the display field
        field_classes = {'away_team': TeamDisplayField,
                         'home_team': TeamDisplayField}


ScoreFormSet = modelformset_factory(Matchup, form=ScoreForm, extra=0)


class YearWeekForm(forms.Form):
    year = forms.CharField(label='',
                           widget=forms.Select(
                               choices=YEAR_CHOICES,
                               attrs={'onChange': 'this.form.submit()'}),
                           )
    week = forms.CharField(label="",
                           widget=forms.Select(
                               choices=WEEK_CHOICES,
                               attrs={'onChange': 'this.form.submit()'})
                           )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['year'].initial = Config.get_year()
        self.fields['week'].initial = Config.get_week()


class YearForm(forms.Form):
    year = forms.CharField(label='',
                           widget=forms.Select(
                               choices=YEAR_CHOICES,
                               attrs={'onChange': 'this.form.submit()'}),
                           )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['year'].initial = Config.get_year()


class WeekForm(forms.Form):
    week = forms.CharField(label="Week", widget=forms.Select(choices=WEEK_CHOICES))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['week'].initial = Config.get_week()


class PicksForm(forms.ModelForm):
    team = forms.ChoiceField(label='', required=False)
    # team_choice = forms.ChoiceField(label='', required=False, widget=forms.RadioSelect)
    team_choice = forms.CharField(required=False)
    # lock = forms.BooleanField(label='', required=False)
    user = forms.ModelChoiceField(get_user_model().objects.none(), label='', required=False, widget=forms.HiddenInput)
    win = forms.BooleanField(label='', widget=forms.HiddenInput(), required=False)
    away_team = forms.CharField(required=False)
    home_team = forms.CharField(required=False)
    away_code = forms.CharField(required=False)
    home_code = forms.CharField(required=False)
    away_espn_id = forms.CharField(required=False)
    home_espn_id = forms.CharField(required=False)
    favorite = forms.CharField(required=False)
    favorite_code = forms.CharField(required=False)
    spread = forms.FloatField(required=False)
    is_open = forms.BooleanField(widget=forms.HiddenInput, required=False)
    date_time_of_game = forms.CharField(required=False)
    username = forms.CharField(required=False)
    matchup_espn_id = forms.CharField(required=False)
    game_info = forms.CharField(required=False)
    current_spread = forms.CharField(required=False)
    over_under = forms.IntegerField(required=False)
    weather = forms.CharField(required=False)
    high_temp = forms.IntegerField(required=False)

    class Meta:
        model = Pick
        fields = ['lock']

    def __init__(self, *args, **kwargs):
        super(PicksForm, self).__init__(*args, **kwargs)

    def save(self, commit=True, *args, **kwargs):
        pick = super(PicksForm, self).save(commit=False, *args, **kwargs)
        team_choice = self.cleaned_data['team_choice']
        if team_choice is not None:
            pick.team = Team.objects.filter(code=team_choice).first()
        if commit:
            pick.save()


class BasePicksFormSet(BaseModelFormSet):
    one_open = False  # Used to demark if any of the picks in the form are able to be updated
    # when the commissioner makes picks on behalf of another user, the username field haas that user's username
    username = forms.CharField(label='', required=False)

    def clean(self):
        lock_count = self.count_locks()
        if lock_count > 1:
            raise forms.ValidationError('You have {} locks.  Pick one matchup for your lock'.format(lock_count))

    def count_locks(self):
        lock_count = 0
        for form in self.forms:
            if form.cleaned_data['lock']:
                lock_count += 1
        return lock_count


PicksFormSet = modelformset_factory(Pick, form=PicksForm, extra=0, formset=BasePicksFormSet)


class ConfigForm(forms.ModelForm):
    year = forms.CharField(widget=forms.Select(choices=YEAR_CHOICES))
    week = forms.CharField(widget=forms.Select(choices=WEEK_CHOICES))

    class Meta:
        model = Config
        fields = ['year', 'week']
        exclude = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['year'].initial = Config.get_year()
        self.fields['week'].initial = Config.get_week()


class TextForm(forms.Form):
    message = forms.CharField(widget=forms.Textarea(attrs={'rows': 5, 'cols': 20}))
