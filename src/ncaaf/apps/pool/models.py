"""Provides the core logic for the football pool.

There is a diagram of the object model here: https://www.collegefbpool.com/static/img/object_model.png
"""
from datetime import timedelta, datetime
import pytz
import logging
from random import randint

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Q, Sum, Count
from django.utils import timezone

from ..member.models import Profile, SMS

__author__ = "Kenny Schmidt"
__email__ = "kwschmidtjr@gmail.com"
__status__ = "Production"

# Order for re-loading Models from Excel
# 1. Users
# 2. Profiles
# 3. Teams
# 4. Matchups
# 5. Picks
# 6. WeeklyStats


# TODO Remove this import and flag
# The following statements in main urls.py cause initial migration failure
# from ncaaf.apps.pool.views import home
# path('pool/', include('ncaaf.apps.pool.urls')),
# path('', home, name='home'),
# Related issues https://stackoverflow.com/questions/40549437/django-migration-relation-does-not-exist
# https://stackoverflow.com/questions/38383513/why-are-my-django-migrations-loading-my-urls-py

from django.core.management.base import BaseCommand

BaseCommand.requires_system_checks = False

CONFERENCE_CHOICES = (
    ('AAC', 'American'),
    ('ACC', 'ACC'),
    ('B1G', 'Big 10'),
    ('B12', 'Big 12'),
    ('CUSA', 'Conference USA'),
    ('IND', 'Independent'),
    ('MAC', 'MAC'),
    ('MWC', 'Mountain West'),
    ('P12', 'Pac-12'),
    ('SEC', 'SEC'),
    ('SBC', 'Sunbelt')
)

WEEK_CHOICES = (
    ('0', 'Week 0'),
    ('1', 'Week 1'),
    ('2', 'Week 2'),
    ('3', 'Week 3'),
    ('4', 'Week 4'),
    ('5', 'Week 5'),
    ('6', 'Week 6'),
    ('7', 'Week 7'),
    ('8', 'Week 8'),
    ('9', 'Week 9'),
    ('10', 'Week 10'),
    ('11', 'Week 11'),
    ('12', 'Week 12'),
    ('13', 'Week 13'),
    ('14', 'Week 14'),
    ('15', 'Week 15'),
    ('16', 'Week 16'),
)

# Expanded through 2025
YEAR_CHOICES = (
    ('2019', '2019'),
    ('2020', '2020'),
    ('2021', '2021'),
    ('2022', '2022'),
    ('2023', '2023'),
    ('2024', '2024'),
    ('2025', '2025'),
)


class Config(models.Model):
    """Config holds key parameters that impact the operation of the application"""
    year = models.PositiveIntegerField()  # The current season / year
    week = models.PositiveIntegerField()  # The current week of the season (1-15)
    heartbeat = models.BooleanField(default=False)  # Toggle to turn on/off sending the heartbeat text messages
    batches = models.BooleanField(default=False)  # Toggle to turn on/off the batch processing

    @staticmethod
    def get_year():
        config = Config.objects.first()
        if config is None:
            return 2010  # the only time this happens is when running tests
        else:
            return config.year

    @staticmethod
    def get_week():
        config = Config.objects.first()
        if config is None:
            return 14  # the only time this happens is when running tests
        else:
            return config.week

    @staticmethod
    def send_heartbeat():
        config = Config.objects.first()
        if config is None:
            return False  # the only time this happens is when running tests
        else:
            return config.heartbeat

    @staticmethod
    def run_batches():
        config = Config.objects.first()
        if config is None:
            return False  # the only time this happens is when running tests
        else:
            return config.batches


class Team(models.Model):
    """A Team is a college football team"""
    code = models.CharField(max_length=5, default='')
    name = models.CharField(max_length=30, default='')
    mascot = models.CharField(max_length=30, default='')
    conference = models.CharField(max_length=9, default='')
    division = models.CharField(max_length=2, default='')
    espn_id = models.CharField(max_length=5, default='')

    def __str__(self):
        return self.code


class Matchup(models.Model):
    """A Matchup is a football game between two teams during a particular college football season.

    Matchups are set by the pool commissioner.  At the moment Matchups are loaded by first obtaining the
    ESPN ID for the matchup and then using stats/batch_runner to load the key data from the ESPN API.
    The commissioner then enters the opening line (opening line to be automated in future release).
    S/he then releases the matchups to the users to make their picks.  Users can make their picks until
    1 hour before kickoff (weekday games) and by 5pm ET on Fridays for weekend games.  After that point,
    the picks are locked.  After the picks are locked, the system will get live scores from the
    via the stats app.  Once the live scores indicates the game is over, the matchup moves to completed state.

    State Transitions
    * EXCLUDED - Upon first load, all potential matchups are in EXCLUDED state.  The Commissioner
        selects which matchups will be in the pool (by setting to PICKS state) that week and enters the
        opening line and the favorite.  Matchups not moved to PICKS state will remain in EXCLUDED state indefinitely.
    * PICKS - In this state, Pick objects can be attached.  The system will move matchups to LOCKED on a schedule.
        Current implementation is all picks move to LOCKED on Friday at 5pm ET.  For any matchups during the week,
        these move to LOCKED one hour prior to kickoff.
    * LOCKED - In this state, the system updates the scores from the ESPN live scores feed.  Upon determining the
        matchup/game is over from the feed, the system will move these to COMPLETE
    * COMPLETE - This is the final state of the object.
    """
    # States
    EXCLUDED = 'EXCLUDED'
    PICKS = 'PICKS'
    LOCKED = 'LOCKED'
    COMPLETE = 'COMPLETE'
    MATCHUP_STATE = (
        (EXCLUDED, 'Not in pool'),
        (PICKS, 'Ready for picks'),
        (LOCKED, 'Ready for the game'),
        (COMPLETE, 'Game complete'),
    )
    year = models.PositiveIntegerField()  # From Config.year when model created
    week = models.PositiveIntegerField()  # From Config.week when model created
    date_time_of_game = models.DateTimeField(null=True)  # From ESPN schedule / game feed
    home_team = models.ForeignKey(Team, related_name='+', on_delete=models.CASCADE)  # From ESPN schedule / game feed
    away_team = models.ForeignKey(Team, related_name='+', on_delete=models.CASCADE)  # From ESPN schedule / game feed
    home_score = models.PositiveIntegerField()  # From ESPN live scores feed
    away_score = models.PositiveIntegerField()  # From ESPN live scores feed
    opening_line = models.FloatField()  # Entered by Commissioner
    favorite = models.ForeignKey(Team, related_name='+', on_delete=models.CASCADE, null=True)  # Entered by Commissioner
    state = models.CharField(max_length=8, choices=MATCHUP_STATE, default=EXCLUDED)  # Maintained by model
    espn_id = models.CharField(max_length=10, default='')  # From ESPN schedule / game feed

    # These attributes are all meant to be display only and there is no current logic based on these attributes.
    # This information is all retrieved from the ESPN live feed and updated on by a recurring batch process.
    # As the number of such information-only attributes expands, design alternatives could be considered so that
    # this model doesn't continue to grow.
    # 1. Use ESPN directly instead of storing a copy here.  I've not yet done this because I want to ensure I still
    #    have this data historically even if ESPN changes or discontinues their API.
    # 2. Use a TextField and store as a JSON object.  This would allow flexibility in adding new attributes without
    #    expanding the model.  Of course the read methods would be slightly more complex.
    game_info = models.CharField(max_length=20, default='', blank=True)
    current_spread = models.CharField(max_length=15, default='', blank=True)
    over_under = models.PositiveIntegerField(null=True, blank=True)
    weather = models.CharField(max_length=25, default='', blank=True)
    high_temp = models.IntegerField(null=True, blank=True)

    class Meta:
        ordering = ['date_time_of_game']

    def __str__(self):
        if self.favorite is None:
            favorite = '----'
        else:
            favorite = self.favorite.code
        return '{} at {} {}, fav={}, state={}'.format(
            self.away_team.code, self.home_team.code, self.game_info, favorite, self.state)

    def short_str(self):
        return '{} @ {}'.format(self.away_team, self.home_team)

    def set_open_for_picks(self):
        """Sets the matchup to PICKS state.  Used when matchup is set and then released for players to make picks."""
        self.state = Matchup.PICKS
        super().save()

    def is_open_for_picks(self):
        """Returns true of the matchup is in PICKS state."""
        if self.state == Matchup.PICKS:
            return True
        else:
            return False

    def set_locked(self):
        """Sets the matchup to LOCKED state.  Used when matchup is closed for players to make picks."""
        self.state = Matchup.LOCKED
        super().save()

    def is_locked(self):
        """Returns true of the matchup is in LOCKED state."""
        if self.state == Matchup.LOCKED:
            return True
        else:
            return False

    def set_complete(self):
        """Sets the matchup to COMPLETE state.  Used when the game is finished and after the final scores entered."""
        self.state = Matchup.COMPLETE
        super().save()

    def is_complete(self):
        """Returns true of the matchup is in COMPLETE state."""
        if self.state == Matchup.COMPLETE:
            return True
        else:
            return False

    def is_in_pool(self):
        """Returns true if the matchup is in the pool for this week"""
        if self.state != Matchup.EXCLUDED:
            return True
        else:
            return False

    @staticmethod
    def move_to_locked(dt, year, week):
        """For all matchups ready to get locked, locks them and does auto picks for those users who want it

        The following occurs for each matchup:

        - If the current time is within one hour of game time, or
        - If the current time is after Friday 5pm ET
        - Then the matchup is moved to LOCKED.  Also, if any user wanted an auto_pick, that is done

        It is expected this would be called by a batch process regularly throughout the week.

        :param datetime dt: an aware date/time object in UTC
        :param int year: the year that contains the week to be calculated
        :param int week: the week number (1-15) of the college football season
        :raises exception if dt is not UTC
        :returns int number of matchups moved to locked
        """
        assert dt.tzinfo is pytz.utc, 'Matchup.move_to_locked() expected UTC but received {}'.format(dt.tzinfo)
        matchups_flipped = 0
        matchup_list = Matchup.get_in_pool_matchups(year, week)
        for matchup in matchup_list:
            if matchup.date_time_of_game is None:
                continue  # A matchup not yet scheduled for a date/time can't be moved to LOCKED
            gt = matchup.date_time_of_game - TZ.HOUR
            cutoff = TZ.weekly_cutoff_time(dt)
            if dt > gt or dt > cutoff:
                if matchup.is_locked():
                    pass  # No need to change to locked if it already is
                elif matchup.is_complete():
                    pass  # Don't move state backwards to LOCKED for COMPLETED matchups
                else:
                    matchups_flipped += 1
                    matchup.set_locked()
                    matchup.save()
                    Pick.auto_pick(matchup)
        return matchups_flipped

    @staticmethod
    def notify_pick_needed(dt, year, week):
        """Notifys user for all matchups where a user hasn't made a pick and there is one hour left.

        The following occurs for each matchup:

        - If the current time is within three hours of game time, or after Friday 3pm ET
        - AND a user hasn't yet made a pick
        - THEN s/he is notified to make the pick

        It is expected this would be called by a batch process regularly throughout the week.

        :param datetime dt: an aware date/time object in UTC
        :param int year: the year that contains the week to be calculated
        :param int week: the week number (0-16) of the college football season
        :raises exception if dt is not UTC
        :returns int number of notifications sent
        """
        # assert dt.tzinfo is pytz.utc, 'Matchup.move_to_locked() expected UTC but received {}'.format(dt.tzinfo)
        notifications_sent = 0
        matchup_list = Matchup.get_in_pool_matchups(year, week)
        logger = logging.getLogger('ncaaf')
        logger.info('len(matchup_list): {}'.format(len(matchup_list)))
        for matchup in matchup_list:
            if matchup.date_time_of_game is None:
                continue  # A pick isn't due for a matchup not yet scheduled for a date/time
            # Notifies three hours before kickoff for a weekday game (gt)
            # Notifies two hours before Friday cutoff
            gt = matchup.date_time_of_game - TZ.HOUR - TZ.HOUR - TZ.HOUR
            cutoff = TZ.weekly_cutoff_time(dt) - TZ.HOUR - TZ.HOUR
            if dt > gt or dt > cutoff:
                if matchup.is_locked():
                    pass  # No need to notify if matchup locked
                elif matchup.is_complete():
                    pass  # No need to notify if matchup complete
                else:
                    logger.info('Calling Pick.notify_pick_needed for {}'.format(matchup.short_str()))
                    notifications_sent += Pick.notify_pick_needed(matchup)
            else:
                logger.info('{} start time not past cutoff'.format(matchup.short_str()))
        return notifications_sent

    def get_pick_count(self):
        """Returns the number of picks made for this week"""
        pick_list = Pick.objects.filter(matchup=self)
        if pick_list.count() == 0:
            return 0
        count = 0
        for pick in pick_list:
            if pick.team is not None:
                count += 1
        return count

    @staticmethod
    def open_week_for_picks(year, week):
        """Locks the matchups to users can make picks."""
        matchup_list = Matchup.objects.filter(year=year, week=week)
        for matchup in matchup_list:
            matchup.set_open_for_picks()

    @staticmethod
    def get_pick_ready_matchups(year, week):
        """Returns the matchups that are in the specified week's pool and ready for making picks"""
        matchup_list = Matchup.objects.filter(Q(state=Matchup.PICKS), year=year, week=week)
        return matchup_list

    @staticmethod
    def get_game_ready_matchups(year, week):
        """Returns the matchups that are ready for the matchup or the matchup is in progress"""
        matchup_list = Matchup.objects.filter(Q(state=Matchup.LOCKED), year=year, week=week)
        return matchup_list

    @staticmethod
    def get_in_pool_matchups(year, week):
        """Returns the matchups that are part of the week specified"""
        # matchup_list = Matchup.objects.filter(
        #      Q(state=Matchup.LOCKED) | Q(state=Matchup.COMPLETE) | Q(state=Matchup.PICKS),
        #    year=year, week=week)
        # Replaced above code where three states are included with the simpler query below where the fourth
        # state is excluded
        matchup_list = Matchup.objects.filter(year=year, week=week).exclude(state=Matchup.EXCLUDED)
        matchup_list.order_by('picks__user_id')  # for
        return matchup_list

    def winner(self):
        """Determines which team won the matchup against the opening line.

        If the favorite team beat the other team by more points than the opening line, the favorite
        team wins.  Otherwise the other team wins.  For example assume these facts:

        - favorite: home team
        - opening line: 10
        - home team score: 24
        - away team score: 20

        In this example, the winner is the away team.  This is because the home team was favored to win
        by 10 points, but won by only 4.

        :return: a Team object for the winner of the matchup, or None if the game isn't over or it's a push
        """
        if not self.is_complete():
            return None
        if self.favorite == self.home_team:
            if (self.home_score - self.away_score) > self.opening_line:
                winner = self.home_team
            elif (self.home_score - self.away_score) == self.opening_line:
                winner = None
            else:
                winner = self.away_team
        else:
            if (self.away_score - self.home_score) > self.opening_line:
                winner = self.away_team
            elif (self.away_score - self.home_score) == self.opening_line:
                winner = None
            else:
                winner = self.home_team
        return winner


class Pick(models.Model):
    """A Pick is a user's assessment of who will win a Matchup."""
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='+', on_delete=models.CASCADE)
    matchup = models.ForeignKey(Matchup, related_name='picks', on_delete=models.CASCADE)
    team = models.ForeignKey(Team, related_name='+', null=True, on_delete=models.CASCADE)
    lock = models.BooleanField(default=False)
    win = models.BooleanField(default=False)

    class Meta:
        ordering = ['user_id']

    # def save(self, *args, **kwargs):
    #    """Saves the object only if the corresponding Matchup is in setup or open for creating picks.
    #
    #   If the corresponding Matchup is not open for creating picks or in setup, an Exception is raised.
    #    """
    #    if self.matchup.is_open_for_picks():
    #        super().save(*args, **kwargs)
    #    else:
    #        raise ValidationError("Pick can't be saved when associated Matchup is in state {}".format(
    #            self.matchup.state),
    #            code='STATE_VIOLATION'
    #        )

    def __str__(self):
        team = 0 if self.team is None else self.team.code
        return 'Pick id {} is {} for {}'.format(self.id, team, self.matchup)

    def is_open(self):
        """Returns True if the matchup is ready for users to make picks"""
        if self.matchup.is_open_for_picks():
            return True
        else:
            return False

    def is_winner(self):
        """Returns true if the pick was correct"""
        if self.team is None:
            return False  # the user hasn't made a pick

        return self.team == self.matchup.winner()            

    def points(self):
        """Calculates the number of points earned by this pick.

        If the pick is correct and is a lock, three points were earned.  If the pick is correct and is
        not a lock, one point was earned.  Otherwise, zero points were earned.

        :return: int either 0, 1, or 3
        """
        if self.team is None:
            return 0  # the user hasn't made a pick

        if self.team == self.matchup.winner():
            if self.lock:
                return 3
            else:
                return 1
        else:
            return 0

    @staticmethod
    def points_for_week(user, year, week):
        """Calculates the number of points the user accumulated for this week's betting.

        :param User user: the user to calc points
        :param int year: the year that contains the week to be calculated
        :param int week: the week number (0-16) of the college football season
        :return int: the number of points the user earned during the week
        """
        pick_list = Pick.objects.filter(user=user, matchup__year=year, matchup__week=week)
        points = 0
        for pick in pick_list:
            points = points + pick.points()
        return points

    @staticmethod
    def get_picks_for_week(user, year, week):
        """Returns has all the choices (Picks) that the user made for the specified week.

        If the user has not yet made any picks, Pick objects will be created for each matchup for the week
        If there are no matchups yet created, will return an empty Queryset.  If there are no matchups
        yet set up for this week, will return an empty Queryset.

        :param User user: the user whose picks are being returned
        :param int year: the year that contains the week to be queried
        :param int week: the week number (0-16) of the collage football season
        :return: Queryset: containing all the picks for the user for the specified week
        """
        # Get all the matchups for the week specified.  Then build a query for all
        # the Picks that contain those matchups and match the logged-in user
        matchup_list = Matchup.get_pick_ready_matchups(year, week)
        if matchup_list.count() == 0:
            return Pick.objects.none()
        pick_filter = Q()
        for matchup in matchup_list:
            pick_filter.add(Q(matchup=matchup.id), Q.OR)
        pick_filter.add(Q(user=user), Q.AND)
        pick_list = Pick.objects.filter(pick_filter).order_by('matchup__date_time_of_game', 'matchup_id')
        if pick_list.count() != matchup_list.count():
            # this will occur the first time a user tries to enter picks for the week or if the commissioner
            # adds a new matchup after some picks have occurred
            for matchup in matchup_list:
                found_pick = False
                for pick in pick_list:
                    if pick.matchup == matchup:
                        found_pick = True
                if not found_pick:
                    Pick.objects.create(user=user, matchup=matchup)
                    pick_list = Pick.objects.filter(pick_filter)
        return pick_list

    def get_teams_for_pick(self):
        """Returns the two teams that are in the matchup that relates to this Pick.

        :return: Queryset: of exactly two Team objects
        """
        team_filter = Q()
        team_filter.add(Q(pk=self.matchup.away_team.id), Q.OR)
        team_filter.add(Q(pk=self.matchup.home_team.id), Q.OR)
        teams = Team.objects.filter(team_filter)
        return teams

    @staticmethod
    def make_empty_picks(year, week):
        """Creates a set of Pick objects for the year and week specified.

        The Pick objects will have all attributes populated with exception of team and lock. For any Pick object that
        already exists (eg. user has already made some picks), a duplicate will not be created.

        :param int year: the year/season the matchup occurs
        :param int week: the week number (1-15) of the collage football season
        :return: nothing
        """
        user_model = get_user_model()
        user_list = user_model.objects.all()
        for user in user_list:
            # this method does all the work we need
            Pick.get_picks_for_week(user, year, week)

    @staticmethod
    def auto_pick(matchup):
        """Makes picks for the supplied matchup for every user who has auto_pick in their profile

        For each matchup, the pick is only made if the user has not previously specified a team for their pick

        :param Matchup matchup: the matchup upon which to make auto picks
        :return: nothing
        """
        pick_list = Pick.objects.filter(matchup=matchup)
        for pick in pick_list:
            if pick.team is not None:
                continue  # if a pick is already made, then don't auto pick
            try:
                user_profile = Profile.objects.get(user=pick.user)
            except Profile.DoesNotExist:
                continue  # if a user doesn't yet have a profile, then auto_pick is by default false
            if user_profile.auto_pick:
                home_away = randint(0, 1)
                if home_away == 0:
                    pick.team = matchup.home_team
                else:
                    pick.team = matchup.away_team
                pick.save()

    @staticmethod
    def notify_pick_needed(matchup):
        """Notifies each user if s/he still needs to make a pick for the specified matchup

        :param Matchup matchup: the matchup upon which to notify users
        :return: int number of notifications sent
        """
        logger = logging.getLogger('ncaaf')
        if not matchup.is_open_for_picks():
            logger.info('matchup not open for picks')
            return 0
        pick_list = Pick.objects.filter(matchup=matchup)
        logger.info('len(pick_list): {}'.format(len(pick_list)))
        notifications_sent = 0
        for pick in pick_list:
            try:
                user_profile = Profile.objects.get(user=pick.user)
            except Profile.DoesNotExist:
                logger.info('no user profile')
                continue  # if a user doesn't yet have a profile, then text_reminder is by default false
            if pick.team is None and user_profile.text_reminder:
                notifications_sent += 1
                message = 'Pick needed: {}'.format(matchup.short_str())
                tag = user_profile.mobile_phone + "|" + matchup.espn_id
                logger.info('calling SMS.queue_message for {} with: {}'.format(
                    user_profile.user, message))
                result = SMS.queue_message(message, user_profile.mobile_phone, tag)
                logger.info('result of SMS.queue_message is: {}'.format(result))
        return notifications_sent


class WeeklyStats(models.Model):
    """A WeeklyStat the number of points a user earned from his/her Picks in a week of a football season."""
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='+', on_delete=models.CASCADE)
    year = models.PositiveIntegerField()  # The year of the season game played
    week = models.PositiveIntegerField()  # The week number of the game played
    lock = models.BooleanField()  # True if the user got his/her lock this week
    points = models.PositiveIntegerField()  # Number of points user earned this week
    num_matchups = models.PositiveIntegerField(default=0)  # Number of matchups the user picked this week

    # Note there is slight denormalization in num_matchups.  The number of matchups in the week is the same for all
    # users.  If there are 10 users in the pool, then num_matchups will be the same for each of the 10 users in their
    # corresponding WeeklyStats objects.

    class Meta:
        ordering = ['user']

    def percent_wins(self):
        """Returns the percentags of wins for this user for this week"""
        if self.lock:
            return (self.points - 2) / self.num_matchups
        else:
            return self.points / self.num_matchups

    def percent_locks(self):
        if self.lock:
             return 1 / self.num_matchups
        else:
            return 0

    @staticmethod
    def calc_weekly_stats(year, week):
        """Calculates the stats for the year and week specified.  Saves results

        This should be triggered from an automated batch process

        :returns int representing the number of stats updated.
        """
        user_model = get_user_model()
        user_list = user_model.objects.all()
        count = 0
        for user in user_list:
            pick_list = Pick.objects.filter(user=user, matchup__year=year, matchup__week=week)

            points = 0
            lock = False
            for pick in pick_list:
                if pick.matchup.is_complete():
                    points = points + pick.points()
                    if pick.points() == 3:
                        lock = True
                    if pick.is_winner():
                        if pick.win != True:
                            # This will run numerous times in a week.  No need to save the win status every time.
                            # After the first time pick.win is set to True and saved, this code won't run again.
                            pick.win = True
                            pick.save()
            num_matchups = pick_list.count()
            count += 1
            stats_list = WeeklyStats.objects.filter(user=user, year=year, week=week)
            if stats_list.count() == 0:
                # A stats object for this week has not yet been created so create one
                stats = WeeklyStats(user=user, year=year, week=week, lock=lock, points=points, num_matchups=num_matchups)
                stats.save()
            elif stats_list.count() == 1:
                # A stats object for this week has been created so just update it
                stats = stats_list.first()
                stats.points = points
                stats.lock = lock
                stats.num_matchups = num_matchups
                stats.save()
            else:
                # TODO: raise an exception
                count = 0
                return count
        return count

    @staticmethod
    def get_stats(year):
        """Prepares a grid of all the points earned by each user for each week in the football pool.

        There is one row for each week and one column for each user.

        :param year - the season (year) to calculate the stats
        :return: user_list - a Queryset of all the users of the application
        :return: stats_list - a two-dimensional array of points earned by each user in each week.  The
            columns are each user and the rows are each week of the season.  There are no column headers.
            The order of the columns is the same as the order of the users returned in user_list.
        :return: total_points - an array of the total points for each user in the season.  The order of the
            elements is the same as the order of the users returned in user_list
        :return: total_locks - an array of the total locks for each user in the season.   The order of the
            elements is the same as the order of the users returned in user_list
        """
        user_model = get_user_model()
        user_list = user_model.objects.all()
        user_count = user_list.count()
        stats_list = [[]]  # rows will be the weeks and columns will be the users
        total_points = [0] * user_count  # contains the total # of points for the users in the football pool
        total_locks = [0] * user_count  # contains the total # of locks for the users in the football pool

        # Cycle through all weeks of the football season and add the number of points each person has accumulated
        for week in range(len(WEEK_CHOICES)-1):
            stats_objs = WeeklyStats.objects.filter(year=year, week=week)
            if (stats_objs.count() == 0) & (week > 0):
                # No need to count stats for weeks where we don't yet have stats
                # Some years didn't have a week 0 so need to get past that one at least
                break

            stats_objs.order_by('user')
            # Add all the stats for this week to the stats list
            # Note that stats_list is zero-based so need to subtract one from the week
            stats_list[week].extend(stats_objs)
            if week < len(WEEK_CHOICES):
                stats_list.append([])

            # Update the accumulated points for each user for this week
            i = 0
            for stat in stats_objs:
                total_points[i] += stat.points
                if stat.lock:
                    total_locks[i] += 1
                i += 1

        return user_list, stats_list, total_points, total_locks

    @staticmethod
    def calc_pick_stats(user, year=0, count=5, min_games=1):
        """Determines the top N teams the user picked over the course of the specified time period.

        The return value is a dictionary
        {
            "user_id": "kwschmidt",
            "user_name": "Kenny",
            "profile_image": "profile_image/user_8_zAkufcd.jpg"
            "year_display": "All Years" | "Year 2019" | "Year 2020" | etc.,
            "top_wins" : [                  // An array of 0-N teams depending on the count parameter
                {
                    "team_code": "ALA",     // Any of the 100s of team codes in the system
                    "team_espn_id": "12345"
                    "total_picks": 23,      // number of times this team was picked
                    "total_wins": 15,       // number of times the pick was a winner
                    "total_losses": 8,      // number of times the pick was a loser
                    "win_percent": 0..100   // integer representing the winnning percentage
                    "loss_percent": 0..100  // integer representing the losing percentage

                }
            ]
            "top_lossses" : [
                same dictionary as above
            ]
        }

        :param user - the user for whom to calculate the stats.
        :param year - if 0, the stats will be calculated across all years.  Othersiwe the stats will be calculated
            only for the year specified. 
        :param count - the number of teams to return and count must be > 0.  One team will always be returned even if
            0 or a negative number is sent as a parameter.  The only exception is at the beginning of a season when 
            no picks have been made.  In this case 0 will be returned.  If there are ties in the number of wins (or 
            losses), the team returned could be > count.  For example if count==2 and the 2nd and 3rd teams have the 
            same number of wins (or losses), three teams will be returned.
        :param min_games - the minimum number of times a team has to be picked in order to be included in the stats.
            This will be set to 1 if min_games < 1.

        :return stats - a dictionary of results
        """
        # Clean up parameters
        if count < 1:
            count = 1

        if min_games < 1:
            min_games = 1

        pick_query = Pick.objects.filter(user=user)
        if year == 0:
            year_display = "All Years"
        else:
            pick_query = pick_query.filter(matchup__year=year)
            year_display = "Year {}".format(year)

        result = {
            "user_id": user.username,
            "user_name": user.first_name,
            "profile_image": "{}".format(user.profile.image),
            "year_display": year_display,
            "top_wins": [],
            "top_losses": [],
        }

        l = pick_query.count()
        result['top_wins'] = WeeklyStats.__top_wins_losses(pick_query, True, count)
        result['top_losses'] = WeeklyStats.__top_wins_losses(pick_query, False, count)

        return result

    @staticmethod
    def __top_wins_losses(pick_query, win, count):

        win_loss_query = pick_query.filter(win=win).values('team').annotate(win_loss=Count('team')).order_by('-win_loss')

        l = win_loss_query.count()
        result = []
        max = 0
        for pick in win_loss_query:
            if max == 0:
                prior_total = pick['win_loss']

            if prior_total != pick['win_loss'] and max >= count:
                break

            total_picks = pick_query.filter(team=pick['team']).count()
            team = Team.objects.filter(id=pick['team']).first()

            # Happens when the team was picked N teams, but didn't win any (win parameter == True), or didn't lose 
            # any (win parameter == False)
            if pick['win_loss'] == 0:
                total_win_loss = 0
            else: 
                total_win_loss = pick['win_loss']

            if win:
                total_wins = total_win_loss
                total_losses = total_picks - total_wins
            else:
                total_losses = total_win_loss
                total_wins = total_picks - total_losses
            
            if team is not None:
                result.append(
                    {
                        "team_code": team.code,
                        "team_espn_id": team.espn_id,
                        "total_picks": total_picks, 
                        "total_wins": total_wins,
                        "total_losses": total_losses,
                        "win_percent": int(total_wins / total_picks * 100),
                        "loss_percent": int(total_losses / total_picks * 100) ,
                    }
                )

            max += 1
            prior_total = pick['win_loss']

        if win:
            result = sorted(result, key=lambda i: (i['total_wins'], i['total_picks']), reverse=True)
        else:
            result = sorted(result, key=lambda i: (i['total_losses'], i['total_picks']), reverse=True)

        return result
        
        ##########################
        # Saving this code that allows sort by win or loss percentage.  To get these it is necessary to query
        # all teams, calculate the win or loss percentage, sort, and then return the top teams by percentage.
        # This is more complex than the implemtation above, which can sort in the database and return top teams
        # by total wins or total losses.
        #
        # total_picks_query = pick_query.values('team').annotate(picks=Count('team'))
        # total_picks_query = total_picks_query.filter(picks__gte = min_games)
        # total_wins_query = pick_query.filter(win=True).values('team').annotate(wins=Count('team'))

        # for pick in total_picks_query:
        #     team = Team.objects.filter(id=pick['team']).first()
    
        #     total_picks = pick['picks']

        #     total_wins_subquery = total_wins_query.filter(team=team)
        #     if total_wins_subquery.count() == 0:
        #         total_wins = 0
        #     else: 
        #         total_wins = total_wins_subquery.first()['wins']

        #     total_losses = total_picks - total_wins
        #     team = {
        #         "team_code": team.code,
        #         "team_espn_id": team.espn_id,
        #         "total_picks": total_picks, 
        #         "total_wins": total_wins,
        #         "total_losses": total_losses,
        #         "win_percent": int(total_wins / total_picks * 100),
        #         "loss_percent": int(total_losses / total_picks * 100) ,
        #     }
        #     result['top_wins'].append(team)
        #     result['top_losses'].append(team)

        # result['top_wins'] = sorted(result['top_wins'], key = lambda i: i['win_percent'], reverse=True)
        # result['top_losses'] = sorted(result['top_losses'], key = lambda i: i['loss_percent'], reverse=True)

        # def sort_list(list, sort):
        #     max = 0
        #     for item in list:
        #         if max == 0:
        #             prior_percent = eval(sort)
        #         if max >= count and eval(sort) != prior_percent:
        #             break
        #         prior_percent = eval(sort)
        #         max += 1
        #     return list[0:max]

        # result['top_wins'] = sort_list(result['top_wins'], "item['win_percent']")
        # result['top_losses'] = sort_list(result['top_losses'], "item['loss_percent']")

        # return result

    @staticmethod
    def calc_pick_and_lock_stats(user, year=0):
        """Calculates the pick percentage correct and lock percentage correct for the user and year specified.

        The return value is a dictionary
        {
            "user_id": "kwschmidt",
            "user_name": "Kenny",
            "profile_image": "profile_image/user_8_zAkufcd.jpg",
            "year_display": "All Years" | "Year 2019" | "Year 2020" | etc.,
            "picks_label": "Correct picks:",
            "locks_label": "Correct locks:",
            "picks_display": "57%",    // whatever is the appropriate amount
            "locks_display": "43%",    // whatever is the appropriate amount
            "correct_picks": <int>,    // 0..100
            "correct_locks": <int>,    // 0..100
        }

        :param user - the user for whom to calculate the stats.
        :param year - if 0, the stats will be calculated across all years.  Othersiwe the stats will be calculated
            only for the year specified. 

        :return stats - a dictionary of results
        """

        # Take a set of WeeklyStats objects, for a user w.  The total correct picks is:
        #     [The sum of all w.points for the set - (2 * the sum of all w.lock) ] / the sum of all w.num_matchups
        # The reason to subtract 2 * locks is because each lock is three points.  Subtracting two points for each
        # from the total points will get back to the correct number of picks.

        # Taking the same wet of Weekly stats objects, w, the correct locks is:
        #     The sum of all (w.lock == True) / the sum of all count(w) [ the number of weeks ] 

        stats_list = WeeklyStats.objects.filter(user=user)
        if year == 0:
            year_display = "All Years"
        else:
            stats_list = stats_list.filter(year=year)
            year_display = 'Year {}'.format(year)
 
        weeks = stats_list.count()

        if weeks == 0:
            # When there are no stats for the user, the percents must be zero
            pick_percent = 0
            lock_percent = 0
        else:
            num_matchups = stats_list.aggregate(sum=Sum('num_matchups')).get('sum')

            # Points
            stats_list = WeeklyStats.objects.filter(user=user)
            if year != 0:
                stats_list = stats_list.filter(year=year)
            points = stats_list.aggregate(sum=Sum('points')).get('sum')

            # Locks
            stats_list = WeeklyStats.objects.filter(user=user, lock=True)
            if year != 0:
                stats_list = stats_list.filter(year=year)
            locks = stats_list.count()

            # TODO: consider if using Pick.wins makes the logic less complex
            wins = points - (locks * 2)
            
            # Matchups will be zero when a user is added in a later season and thus doesn't have any
            # stats for the season (year) being calculated
            pick_percent = 0 if num_matchups == 0 else wins / num_matchups

            lock_percent = locks / weeks
        
        result = {
            "user_id": user.username,
            "user_name": user.first_name,
            "profile_image": "{}".format(user.profile.image),
            "year_display": year_display,
            "picks_label": "Correct picks:",
            "locks_label": "Correct locks:",
            "picks_display": '{}%'.format(int(pick_percent*100)),
            "locks_display": '{}%'.format(int(lock_percent*100)),
            "correct_picks": int(pick_percent*100),
            "correct_locks": int(lock_percent*100),      
        }

        return result


class Stats(object):
    """A helper object to easily get to stats for a user"""

    def __init__(self, user, year):
        self.user = user
        self.profile = Profile.objects.get(user=user)
        self.year = year
        self.points = self.total_points()
        self.locks = self.total_locks()
        self.most_points = False
        self.most_locks = False
        self.least_points = False
        self.least_locks = False

    @staticmethod
    def all(year):
        """Creates a year-to-date stats summary for all players in the pool.

        The stats available for each player include:

        - Total points earned year-to-date
        - Total locks won year-to-date
        - True/False if the player is the points leader, locks leader, points loser, or locks loser

        :param int year: the year for which to calculate stats.
        :return: list of Stats objects, one for each player in the pool.
        """
        user_model = get_user_model()
        user_list = user_model.objects.all()
        stats_list = []
        most_points = 0
        most_locks = 0
        least_points = 1000  # start arbitrarily high so logic in for loop easier
        least_locks = 15  # start higher than possible so logic in for loop easier
        for user in user_list:
            stats = Stats(user, year)
            stats_list.append(stats)
            # keep track of the most number and the least number of both points and locks
            if stats.points > most_points:
                most_points = stats.points
            if stats.locks > most_locks:
                most_locks = stats.locks
            if stats.points < least_points:
                least_points = stats.points
            if stats.locks < least_locks:
                least_locks = stats.locks

        for stat in stats_list:
            # assign points/locks leader and points/locks loser as appropriate.  Could be > 1 leader if tied.
            if stat.points == most_points:
                stat.most_points = True
            if stat.locks == most_locks:
                stat.most_locks = True
            if stat.points == least_points:
                stat.least_points = True
            if stat.locks == least_locks:
                stat.least_locks = True

        return stats_list

    def total_points(self):
        """Returns the total points earned by this user season-to-date

        :return: int
        """
        if WeeklyStats.objects.filter(year=self.year).count() == 0:
            # This will occur at the beginning of a season when no games have been played and thus no stats
            return 0
        else:
            pts = WeeklyStats.objects.filter(user=self.user, year=self.year).aggregate(points_sum=Sum('points'))
            return pts['points_sum']

    def total_locks(self):
        """Returns the total number of locks won by this year season-to-date

        :return: int
        """
        if WeeklyStats.objects.filter(year=self.year).count() == 0:
            # This will occur at the beginning of a season when no games have been played and thus no stats
            return 0
        else:
            stats = WeeklyStats.objects.aggregate(
                locks=Count('pk', filter=Q(user=self.user) & Q(year=self.year) & Q(lock=True))
            )
            return stats['locks']


class TZ(object):
    """Class to help translate between Eastern Time and UTC.

    Because the project is in UTC and all NCAA Football games are in ET, this provides some methods to help
    convert between the two.
    """
    # The LocalTimeZone example at: https://docs.python.org/3/library/datetime.html has more capability than is needed
    # for this project. Because this project only needs Eastern Time for future dates this TZ implementation does not
    # need to handle historical changes to the start and end of DST in the US.

    # In the US, since 2007, DST starts at 2am (standard time) on the second
    # Sunday in March, which is the first Sunday on or after Mar 8.
    DST_START = datetime(1, 3, 8, 2)
    # and ends at 2am (DST time) on the first Sunday of Nov.
    DST_END = datetime(1, 11, 1, 2)

    # Cutoff for making picks is Fridays at 5pm ET
    CUTOFF_DAY = 4
    CUTOFF_TIME = 17

    GAME_DAY = 5  # Saturday is the day most NCAA Football games are played

    EST_OFFSET = timedelta(hours=-5)
    HOUR = timedelta(hours=1)

    @staticmethod
    def utc_offset(dt=timezone.now()):
        """Returns a timedelta object that represents the offset for ET, adjusting for DST as appropriate.

        :param datetime dt: an aware date/time object in UTC.  Default is now

        :return: timedelta object representing difference between UTC and ET (EST or EDT)
        """
        assert dt.tzinfo is pytz.utc, 'TZ.utc_offset expected UTC but received {}'.format(dt.tzinfo)

        dt = dt.replace(tzinfo=None)
        dst_start = TZ.first_sunday_on_or_after(TZ.DST_START.replace(year=dt.year))
        dst_end = TZ.first_sunday_on_or_after(TZ.DST_END.replace(year=dt.year))
        if dst_start + TZ.HOUR <= dt < dst_end - TZ.HOUR:
            return TZ.EST_OFFSET + TZ.HOUR
        else:
            return TZ.EST_OFFSET

    @staticmethod
    def next_sat_noon(dt=timezone.now()):
        """Returns noon ET for the next Saturday in the calendar from dt.

        :param datetime dt: an aware date/time object in UTC.  Default is now

        :return: datetime object in UTC with the UTC equivalent time for noon ET.
            If dt is a Saturday, returns the following Saturday.
        """
        assert dt.tzinfo is pytz.utc, 'TZ.next_sat_noon expected UTC but received {}'.format(dt.tzinfo)

        days_to_go = TZ.GAME_DAY - dt.weekday()
        if days_to_go <= 0:
            days_to_go += 7  # Adjusts a Saturday or Sunday to the following Saturday
        sat_noon = datetime(dt.year, dt.month, dt.day, 0, tzinfo=pytz.utc)
        sat_noon += timedelta(days=days_to_go)

        # Because self.utc_offset() is a negative number, start with 12 noon ET and subtract the offset, thus
        # adding the correct hours to get a UTC version of noon ET.
        noon_et = timedelta(hours=12) - TZ.utc_offset()
        sat_noon += noon_et
        return sat_noon

    @staticmethod
    def next_stat_noon_et(dt=timezone.now()):
        """Returns noon ET for the next Saturday in the calendar from dt.

        :param datetime dt: an aware date/time object in UTC.  Default is now

        :return: datetime object in ET for noon ET.  If dt is a Saturday, returns the following Saturday.
        """
        assert dt.tzinfo is pytz.utc, 'TZ.next_sat_noon_et expected UTC but received {}'.format(dt.tzinfo)

        timezone.activate(pytz.timezone('America/New_York'))
        sat_noon = TZ.next_sat_noon(dt)
        local_noon = timezone.localtime(sat_noon)
        timezone.deactivate()
        return local_noon

    @staticmethod
    def weekly_cutoff_time(dt=timezone.now()):
        """Returns Friday 5pm ET for the current football week, which ends Saturday midnight ET.

        If dt is > Sunday 12:00am ET (midnight), then returns the next Friday 5pm ET
        If dt is <= Sunday 12:00am ET (midnight), then returns the current week Friday 5pm ET.  For example,
        if dt is on Saturday, then 5pm ET on the prior day is returned.

        :param datetime dt: an aware date/time object in UTC.  Default is now

        :return: datetime object in ET for Friday 5pm ET.
        """
        days_to_go = TZ.CUTOFF_DAY - dt.weekday()
        if days_to_go < -1:
            days_to_go += 7  # Adjusts a Sunday to the following Friday
        fri_five_pm = datetime(dt.year, dt.month, dt.day, 0, tzinfo=pytz.utc)
        five_pm_delta = timedelta(hours=TZ.CUTOFF_TIME) - TZ.utc_offset()
        # This logic was used when the goal was to choose the following Friday if dt was > Fri 5pm, but
        # that's no longer the goal.  Keeping code here but commented out for future reference.
        # five_pm_hours = five_pm_delta.seconds / 3600
        # if days_to_go == 0:
        #    if dt.hour >= five_pm_hours:
        #        fri_five_pm += timedelta(days=7)
        # else:
        #    fri_five_pm += timedelta(days=days_to_go)
        fri_five_pm += timedelta(days=days_to_go)
        fri_five_pm += five_pm_delta
        return fri_five_pm

    @staticmethod
    def utc_to_et(dt):
        """Adjusts dt by the proper number of hours to convert from UTC to ET

        :param datetime dt: an aware date/time object in UTC

        :return: datetime object in ET
        """
        assert dt.tzinfo is pytz.utc, 'TZ.utc_to_et expected UTC but received {}'.format(dt.tzinfo)

        offset = TZ.utc_offset(dt)
        timezone.activate(pytz.timezone('America/New_York'))
        et = dt - offset
        et = timezone.localtime(et)
        return et

    @staticmethod
    def first_sunday_on_or_after(dt):
        days_to_go = 6 - dt.weekday()
        if days_to_go:
            dt += timedelta(days_to_go)
        return dt
