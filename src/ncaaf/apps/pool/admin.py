from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import Config, Team, Matchup, Pick, WeeklyStats


class ConfigResource(resources.ModelResource):
    class Meta:
        model = Config


@admin.register(Config)
class ConfigAdmin(ImportExportModelAdmin):
    list_display = ('id', 'year', 'week', 'batches', 'heartbeat')


class TeamResource(resources.ModelResource):
    class Meta:
        model = Team


@admin.register(Team)
class TeamAdmin(ImportExportModelAdmin):
    list_display = ('id', 'code', 'name', 'mascot', 'conference', 'division', 'espn_id')


class MatchupResource(resources.ModelResource):
    class Meta:
        model = Matchup


@admin.register(Matchup)
class MatchupAdmin(ImportExportModelAdmin):
    list_display = ('id', 'year', 'week', 'game_info', 'state', 'dt_tm', 'away', 'home', 'opening_line', 'fav',
        'away_score', 'home_score', 'espn_id')

    def dt_tm(self, obj):
        return obj.date_time_of_game

    def home(self, obj):
        return obj.home_team.code

    def away(self, obj):
        return obj.away_team.code

    def fav(self, obj):
        if obj.favorite is None:
            return ""
        else:
            return obj.favorite.code

    def cover(self, obj):
        return obj.winner


class PickResource(resources.ModelResource):
    class Meta:
        model = Pick


@admin.register(Pick)
class PickAdmin(ImportExportModelAdmin):
    list_display = ('id', 'user', 'matchup', 'team', 'lock', 'win')


class WeeklyStatsResource(resources.ModelResource):
    class Meta:
        model = WeeklyStats


@admin.register(WeeklyStats)
class WeeklyStatsAdmin(ImportExportModelAdmin):
    list_display = ('id', 'user', 'year', 'week', 'lock', 'points', 'num_matchups')
