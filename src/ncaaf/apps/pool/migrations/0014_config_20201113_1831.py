from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pool', '0013_auto_20201023_1029'),
    ]

    operations = [
        migrations.AddField(
            model_name='config',
            name='send_heartbeat',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='config',
            name='run_batches',
            field=models.BooleanField(default=False),
        ),
    ]
