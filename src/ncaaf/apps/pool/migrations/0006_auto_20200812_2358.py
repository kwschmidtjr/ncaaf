# Generated by Django 3.0.5 on 2020-08-12 23:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pool', '0005_auto_20200812_1701'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='matchup',
            options={'ordering': ['date_time_of_game']},
        ),
        migrations.AlterModelOptions(
            name='pick',
            options={'ordering': ['matchup__date_time_of_game']},
        ),
    ]
