from django.apps import AppConfig


class PoolConfig(AppConfig):
    name = 'ncaaf.apps.pool'
