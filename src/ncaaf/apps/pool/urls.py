from django.conf.urls import url
from django.contrib.auth.views import LogoutView

from .views import home, teams, Matchups, Scores, week, leaderboard, Picks, PoolTools, CalcStatsFormView, \
    UpdateConfigFormView, SendTextFormView, Card

urlpatterns = [
    url(r'logout/', LogoutView.as_view(next_page='/pool/home/'), name="logout"),
    url(r'home/', home, name='home'),
    url(r'teams/', teams, name='teams'),
    url(r'matchups/', Matchups.as_view(), name='matchups'),
    url(r'scores/', Scores.as_view(), name='scores'),
    url(r'card/', Card.as_view(), name='card'),
    url(r'week/', week, name='week'),
    url(r'leaderboard/', leaderboard, name='leaderboard'),
    url(r'picks/', Picks.as_view(), name='picks'),
    url(r'pool_tools/', PoolTools.as_view(), name='pool_tools'),
    url(r'^calc_stats/submit/', CalcStatsFormView.as_view(), name='calc_stats'),
    url(r'^update_config/submit/', UpdateConfigFormView.as_view(), name='update_config'),
    url(r'^send_text/submit/', SendTextFormView.as_view(), name='send_text'),
    url(r'/', home, name='home'),
]
