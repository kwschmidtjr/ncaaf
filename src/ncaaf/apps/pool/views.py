from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django import forms
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import TemplateView, FormView

from ..member.models import SMS
from .models import Config, Team, Matchup, Pick, WeeklyStats, Stats, TZ, YEAR_CHOICES
from .forms import TeamsForm, MatchupFormSet, MatchupFormSetPlusOne, ScoreFormSet, YearWeekForm, WeekForm, \
    YearForm, PicksFormSet, TeamModelChoiceField, ConfigForm, TextForm


def home(request):
    """Displays the main page for the site."""

    template_name = 'pool/home.html'
    title = 'CFP - Home'

    year = Config.get_year()
    stats_list = Stats.all(year)
    return render(request, template_name,
                  {'stats_list': stats_list,
                   'year': year,
                   'title': title})


def teams(request):
    """Allows the display of teams by football conference.

    This page isn't currently accessible via the menu, but may enable it in the future.
    """
    template_name = 'pool/teams.html'
    title = 'CFP - Teams'
    form = TeamsForm()
    conf = "AAC"
    if request.method == "POST":
        form = TeamsForm(request.POST)
        if form.is_valid():
            conf = form.cleaned_data['conference']
    else:
        form = TeamsForm()

    team_list = Team.objects.filter(conference=conf)
    return render(request, template_name,
                  {'team_list': team_list,
                   'conference': conf,
                   'form': form,
                   'title': title})


class Matchups(View):
    """This view enables the pool commissioner to set up the games for the week."""

    template_name = 'pool/matchups.html'
    title = 'CFP - Matchups'

    @method_decorator(login_required())
    def get(self, request):
        yr = Config.get_year()
        wk = Config.get_week()
        matchup_list = Matchup.objects.filter(year=yr, week=wk)
        matchup_list.order_by('date_time_of_game', 'id')
        formset = MatchupFormSet(queryset=matchup_list)
        formset = Matchups._add_fields(formset, matchup_list)
        return render(request, self.template_name, {'week': wk, 'formset': formset, 'title': self.title})

    @staticmethod
    def _add_fields(formset, matchup_list):
        i = 0
        for form in formset:
            matchup = matchup_list[i]
            choices = [(matchup.away_team.code, matchup.away_team.code),
                       (matchup.home_team.code, matchup.home_team.code)]
            if matchup.favorite is None:
                form.fields['favorite_choice'] = forms.ChoiceField(label='', widget=forms.RadioSelect, choices=choices)
            else:
                form.fields['favorite_choice'] = forms.ChoiceField(label='', widget=forms.RadioSelect, choices=choices,
                                                                   initial=matchup.favorite.code)
            form.fields['away_code'].initial = matchup.away_team.code
            form.fields['home_code'].initial = matchup.home_team.code
            form.fields['away_espn_id'].initial = matchup.away_team.espn_id
            form.fields['home_espn_id'].initial = matchup.home_team.espn_id
            form.fields['opening_line'].initial = matchup.opening_line
            form.fields['pick_count'].initial = matchup.get_pick_count()
            i += 1
        return formset

    @method_decorator(login_required())
    def post(self, request):
        if 'finalize' in request.POST:  # matchups set, send the text
            matchup_list = Matchup.get_matchups_for_week()
            if matchup_list.count() == 0:
                messages.error(request, 'Enter at least one matchup before letting people make their picks.')
            else:
                # Pick.make_empty_picks(yr, wk)
                # Matchup.open_week_for_picks(yr, wk)
                message = 'Matchups are set for the week.' \
                          'Log into https://collegefbpool.com/pool/make_picks to make your picks!'
                SMS.send_message_to_all(message)
                messages.success(request, "Sent a text to the group: {}".format(message))
            return redirect(reverse('matchups'))
        else:
            # formset = MatchupFormSet(request.POST or None)
            formset = MatchupFormSet(request.POST)
            if formset.is_valid():
                formset.save()
                messages.success(request, 'Your matchups are saved!')
                return redirect(reverse('matchups'))
            else:
                for error in formset.errors:
                    messages.error(request, error)
                matchup_list = Matchup.objects.filter(year=Config.get_year(), week=Config.get_week())
                matchup_list.order_by('date_time_of_game', 'id')
                formset = MatchupFormSet(queryset=matchup_list)
                formset = Matchups._add_fields(formset, matchup_list)
                return render(request, self.template_name, {'formset': formset, 'title': self.title})


class Scores(View):
    """View for the scores to be entered for the week.  Deprecated.

    Now that the scores are retrieved from ESPN, this is no longer needed.  Leaving the capability here in case
    something happens to the ESPN API and this is needed on a temporary basis while being fixed.
    """
    template_name = 'pool/scores.html'
    title = 'CFP - Scores'

    @method_decorator(login_required())
    def get(self, request):
        yr = Config.get_year()
        wk = Config.get_week()
        matchup_list = Matchup.objects.filter(year=yr, week=wk)
        formset = ScoreFormSet(queryset=matchup_list)
        i = 0
        for form in formset:
            form.fields['away_code'].initial = matchup_list[i].away_team.code
            form.fields['home_code'].initial = matchup_list[i].home_team.code
            form.fields['away_espn_id'].initial = matchup_list[i].away_team.espn_id
            form.fields['home_espn_id'].initial = matchup_list[i].home_team.espn_id
            i += 1
        return render(request, self.template_name, {'week': wk, 'formset': formset, 'title': self.title})

    @method_decorator(login_required())
    def post(self, request):
        wk = Config.get_week()
        formset = ScoreFormSet(request.POST)
        if formset.is_valid():
            messages.success(request, "The scores are saved!")
            formset.save()
            return redirect(reverse('scores'))
        else:
            return render(request, self.template_name, {'week': wk, 'formset': formset, 'title': self.title})


class Picks(View):
    """This generates the page for the user to make his/her picks for all the matchups in a week."""

    template_name = 'pool/picks.html'
    title = 'CFP - Picks'

    @method_decorator(login_required())
    def get(self, request):
        # There is capability for the commissioner to make picks on behalf of another user.
        # This was used extensively during the parallel production period - when the pool still ran by exchanging
        # Excel documents via email.  During this time the commissioner manually entered everyone's picks into
        # the system.  Now that the site is used exclusively and the emailing of Excel documents is in the past,
        # this feature is no longer used.  I left the capability here just in case the need arises in the future.
        on_behalf = False
        on_behalf_str = request.GET.get('on_behalf')
        user = request.user
        if on_behalf_str is not None:
            if not request.user.is_commissioner:
                messages.error(request, 'Must be a commissioner to make picks on behalf of somebody')
            else:
                username = request.GET.get('username')
                if username is None or username == '':
                    messages.error(request, 'Add user ID to the URL')
                else:
                    user_model = get_user_model()
                    user_list = user_model.objects.filter(username=username)
                    if user_list.count() == 1:
                        user = user_list.first()
                        on_behalf = True
                    else:
                        messages.error(request, 'User {} does not exist'.format(username))

        pick_list = Pick.get_picks_for_week(user, Config.get_year(), Config.get_week())
        pick_list.order_by('matchup__date_time_of_game')
        formset = PicksFormSet(queryset=pick_list)
        formset.username = user.username
        formset = Picks._add_fields(formset, user, pick_list)
        # for form in formset:
        #     print('{} at {}'.format(form.fields['away_team'].initial, form.fields['home_team'].initial))
        return render(request, self.template_name, {'formset': formset, 'title': self.title, 'on_behalf': on_behalf})
        # 'on_behalf_username': user.username})

    @staticmethod
    def _add_fields(formset, user, pick_list):
        """Adds fields that are used for display-only purposes."""
        i = 0
        one_open = False
        for form in formset:
            # this sets all the data needed for a template to use for display
            matchup = pick_list[i].matchup
            choices = [(matchup.away_team.code, matchup.away_team.code),
                       (matchup.home_team.code, matchup.home_team.code)]
            team = pick_list[i].team
            if team is None:
                form.fields['team_choice'] = forms.ChoiceField(label='', widget=forms.RadioSelect, choices=choices,
                                                               required=False)
            else:
                form.fields['team_choice'] = forms.ChoiceField(label='', widget=forms.RadioSelect, choices=choices,
                                                               initial=team.code, required=False)
            form.fields['away_team'].initial = matchup.away_team.name
            form.fields['home_team'].initial = matchup.home_team.name
            form.fields['away_code'].initial = matchup.away_team.code
            form.fields['home_code'].initial = matchup.home_team.code
            form.fields['away_espn_id'].initial = matchup.away_team.espn_id
            form.fields['home_espn_id'].initial = matchup.home_team.espn_id
            form.fields['favorite'].initial = matchup.favorite.name
            form.fields['favorite_code'].initial = matchup.favorite.code
            form.fields['spread'].initial = matchup.opening_line
            form.fields['is_open'].initial = pick_list[i].is_open()
            form.fields['date_time_of_game'].initial = matchup.date_time_of_game + TZ.utc_offset()
            form.fields['username'].initial = user.username
            form.fields['matchup_espn_id'].initial = matchup.espn_id
            form.fields['game_info'].initial = matchup.game_info
            form.fields['current_spread'].initial = matchup.current_spread
            form.fields['over_under'].initial = matchup.over_under
            form.fields['weather'].initial = matchup.weather
            form.fields['high_temp'].initial = matchup.high_temp
            if pick_list[i].is_open():
                one_open = True
            else:
                form.fields['team_choice'].disabled = True
                form.fields['lock'].disabled = True
            i += 1
        formset.one_open = one_open
        return formset

    @method_decorator(login_required())
    def post(self, request):
        formset = PicksFormSet(request.POST)
        on_behalf = False
        if 'save_on_behalf' in request.POST:
            on_behalf = True
            username = request.GET.get('username')
            user_model = get_user_model()
            user = user_model.objects.filter(username=username).first()
        else:
            user = request.user

        pick_list = Pick.get_picks_for_week(user, Config.get_year(), Config.get_week())
        formset = Picks._add_fields(formset, user, pick_list)
        if formset.is_valid():
            formset.save()
            if on_behalf:
                messages.success(request, 'Picks are saved for {}.'.format(user.username))
                redirect_url = reverse('picks')
                redirect_url += '?on_behalf=True&username={}'.format(user.username)
                return redirect(redirect_url)
            else:
                messages.success(request, 'Your picks are saved!')
                return redirect(reverse('picks'))
        else:
            formset.username = user.username
            return render(request, self.template_name, {'formset': formset, 'title': self.title,
                                                        'on_behalf': on_behalf})


def week(request):
    """Sets up data to view the active games for the week.

    One key column is the Game Info column.  This column has different data depending on the state of the Matchup.
    * Matchup not Started - day and time of game
    * Matchup in Progress - quarter game is in and time on the click.  E.g. 4th Quarter, 12:34
    * Matchup Complete - final
    """
    template_name = 'pool/week.html'
    title = 'CFP - This Week'

    yr = Config.get_year()
    wk = Config.get_week()
    if request.method == 'POST':
        form = YearWeekForm(request.POST)
        if form.is_valid():
            yr = form.cleaned_data['year']
            wk = form.cleaned_data['week']
        refresh = None
        admin_view_html = None
    else:
        form = YearWeekForm()
        refresh = request.GET.get('refresh')
        admin_view_html = request.GET.get('admin_view')

    if refresh is None:
        head = ''
    else:
        head = '<meta http-equiv="refresh" content="60"/>'

    if admin_view_html is None:
        admin_view = False
    else:
        admin_view = True

    matchup_list = Matchup.get_in_pool_matchups(yr, wk)
    user_model = get_user_model()
    user_list = user_model.objects.all()  # default order is by id
    stat_list = WeeklyStats.objects.filter(year=yr, week=wk)
    stat_list.order_by('user__id')
    return render(request, template_name,
                  {'matchup_list': matchup_list,
                   'user_list': user_list,
                   'stat_list': stat_list,
                   'year': yr,
                   'week': wk,
                   'form': form,
                   'admin_view': admin_view,
                   'head': head,
                   'title': title})


def leaderboard(request):
    """Prepares a grid of all the points earned by each user for each week in the football pool.

    There is one row for each week and one column for each user.
    """
    template_name = 'pool/leaderboard.html'
    title = 'CFP - Leaderboard'
    yr = Config.get_year()

    if request.method == 'POST':
        form = YearForm(request.POST)
        if form.is_valid():
            yr = form.cleaned_data['year']
    else:
        form = YearForm()

    user_list, stats_list, total_points, total_locks = WeeklyStats.get_stats(yr)

    return render(request, template_name,
                  {'user_list': user_list,
                   'stats_list': stats_list,
                   'total_points': total_points,
                   'total_locks': total_locks,
                   'form': form,
                   'year': yr,
                   'title': title})

class Card(View):
    """This view shows the 'baseball card' for the user."""

    template_name = 'pool/card.html'
    title = 'CFP - Player Stats'

    @method_decorator(login_required())
    def get(self, request):

        # Clean up the year parameter
        year = request.GET.get('year')
        if year is None:
            # Decided to change default to all years rather than the current year
            # yr = Config.get_year()
            # year = '{}'.format(yr)
            yr = 0
            year = 'All'
        elif year == "All":
            yr = 0
        else:
            try:
                yr = int(year)
            except ValueError:
                yr = 0
                year = 'All'

        # Clean up the username parameter
        username = request.GET.get('username')
        user_model = get_user_model()
        if username is None:
            user = request.user
        else:
            user_query = user_model.objects.filter(username=username)
            if user_query.count() == 1:
                user = user_query.first()
            else:
                user = request.user

        path = request.path

        # Create urls for the Year choices
        def url_year(y, u):
            return {'display': y, 'url': path + '?year=' + y + '&username=' + u.username}

        year_list = [url_year('All', user)]
        for year_choice in YEAR_CHOICES:
            year_list.append(url_year(year_choice[0], user))

        # Create urls for the User choices
        def url_user(y, u):
            return {'display': u.profile.short_name, 'url': path + '?year=' + y + '&username=' + u.username}

        user_list = []
        user_query = user_model.objects.all()
        for usr in user_query:
            user_list.append(url_user(year, usr))

        overall_stats = WeeklyStats.calc_pick_and_lock_stats(user, year=yr)
        top_picks = WeeklyStats.calc_pick_stats(user, year=yr, count=5, min_games=4)
        return render(request, 
            self.template_name, 
            {
                'title': self.title,
                'year_list': year_list,
                'user_list': user_list,
                'overall_stats': overall_stats,
                'top_picks': top_picks,
            })


class PoolTools(TemplateView):
    """View for various utilities a pool administrator will need."""

    template_name = 'pool/pool_tools.html'
    title = 'CFP - Pool Tools'

    # I created this to practice handling multiple forms on a single page.  Here are some references.
    # https://www.codementor.io/@lakshminp/handling-multiple-forms-on-the-same-page-in-django-fv89t2s3j
    # I like this one better: https://riptutorial.com/django/example/16667/one-view--multiple-forms
    # Even better: https://krzysztofzuraw.com/blog/2016/two-forms-one-view-django.html
    @method_decorator(login_required())
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context = PoolTools.context_helper(context)
        return self.render_to_response(context)

    @staticmethod
    def context_helper(context, form=None):
        calc_stats_form = WeekForm()
        update_config_form = ConfigForm()
        send_text_form = TextForm()
        if isinstance(form, ConfigForm):
            update_config_form = form
        elif isinstance(form, TextForm):
            send_text_form = form
        elif isinstance(form, WeekForm):
            calc_stats_form = form
        context['calc_stats_form'] = calc_stats_form
        context['update_config_form'] = update_config_form
        context['send_text_form'] = send_text_form
        context['title'] = PoolTools.title
        return context


class CalcStatsFormView(FormView):
    """Handles the Calculate Stats form from the PoolTools view"""
    form_class = WeekForm

    @method_decorator(login_required())
    def post(self, request, *args, **kwargs):
        calc_stats_form = WeekForm(request.POST)
        if calc_stats_form.is_valid():
            year = Config.get_year()
            wk = calc_stats_form.cleaned_data['week']
            count = WeeklyStats.calc_weekly_stats(year, wk)
            messages.success(request, 'A total of {} stats updated successfully for week {}'.format(count, wk))
            return redirect(reverse('pool_tools'))
        else:
            messages.warning(request, 'Please correct the error below')
            return self.render_to_response(self.get_context_data(calc_stats_form=calc_stats_form))


class SendTextFormView(FormView):
    """Handles the Send Text message form from the PoolTools view"""
    form_class = TextForm

    @method_decorator(login_required())
    def post(self, request, *args, **kwargs):
        send_text_form = TextForm(request.POST)
        if send_text_form.is_valid():
            message = send_text_form.cleaned_data['message']
            SMS.send_message_to_all(message)
            messages.success(request, 'Text Sent!')
            return redirect(reverse('pool_tools'))


class UpdateConfigFormView(FormView):
    """Handles the Update Configuration form from the PoolTools view"""
    form_class = ConfigForm

    @method_decorator(login_required())
    def post(self, request, **kwargs):
        update_config_form = ConfigForm(request.POST, instance=Config.objects.first())
        if update_config_form.is_valid():
            update_config_form.save()
            messages.success(request, 'Year/Week saved!')
            return redirect(reverse('pool_tools'))
        else:
            messages.warning(request, 'Please correct the error below')
            return self.render_to_response(self.get_context_data(update_config_form=update_config_form))
