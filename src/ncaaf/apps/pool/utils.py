"""Provides objects that can be used to export data via Excel."""

from openpyxl.styles import Font, PatternFill, Alignment
from openpyxl import Workbook
from tablib import Dataset

from django.contrib.auth import get_user_model

from .admin import TeamResource, MatchupResource, PickResource, WeeklyStatsResource
from .models import Config, Matchup, Pick, WeeklyStats


class ExcelUtility(object):
    """This object is used to create an Excel file of the whole season.

    The Excel will contain one tab for each week of the season.  Within each tab are the matchups and picks each
    user made for that week.

    Usage: workbook = ExcelUtility.create_workbook()
    """

    FONT = Font(name='Calibri', size=10)
    LOCK_FONT = Font(name='Calibri', size=10, bold=True)
    WINNER_FILL = PatternFill(fill_type='solid', start_color='F56600', end_color='F56600')

    # https://stackoverflow.com/questions/6929115/python-xlwt-accessing-existing-cell-content-auto-adjust-column-width

    @staticmethod
    def create_workbook():
        wb = Workbook()
        wb.remove_sheet(wb.active)
        year = Config.get_year()
        for week in range(0, Config.get_week() + 1):
            ExcelUtility.create_sheet(wb, year, week)
        return wb

    @staticmethod
    def create_sheet(wb, year, week):
        ws = wb.create_sheet('Week {}'.format(week))

        matchup_list = Matchup.get_in_pool_matchups(year, week)
        user_model = get_user_model()
        user_list = user_model.objects.all()  # default order is by id
        stat_list = WeeklyStats.objects.filter(year=year, week=week)
        stat_list.order_by('user__id')

        # Set the header row
        ExcelUtility.set_cell(ws['A1'], 'Date')
        ExcelUtility.set_cell(ws['B1'], 'Away')
        ExcelUtility.set_cell(ws['C1'], 'Home')
        column = 4
        for user in user_list:
            ExcelUtility.set_cell(ws.cell(row=1, column=column), user.first_name)
            column += 1

        # Fill in a row for each matchup
        row = 2
        for matchup in matchup_list:
            # "D g:iA"
            ExcelUtility.set_cell(ws.cell(row=row, column=1), matchup.game_info)
            ExcelUtility.set_cell(ws.cell(row=row, column=2), ExcelUtility.format_team(matchup.away_team, matchup))
            ExcelUtility.set_cell(ws.cell(row=row, column=3), ExcelUtility.format_team(matchup.home_team, matchup))
            column = 4
            for pick in matchup.picks.all():
                cell = ws.cell(row=row, column=column, value=ExcelUtility.format_pick(pick))
                if matchup.is_open_for_picks():
                    continue  # Don't want to display anyone's picks until after the matchup is locked
                if pick.team is None:
                    pass
                elif pick.team == matchup.winner():
                    cell.fill = ExcelUtility.WINNER_FILL

                if pick.lock:
                    cell.font = ExcelUtility.LOCK_FONT
                else:
                    cell.font = ExcelUtility.FONT
                column += 1
            row += 1

        # Fill in the weekly stats
        row += 1
        ExcelUtility.set_cell(ws.cell(row=row, column=3), 'Lock')
        column = 4
        for stat in stat_list:
            if stat.lock:
                ExcelUtility.set_cell(ws.cell(row=row, column=column), 2)
            column += 1

        row += 1
        ExcelUtility.set_cell(ws.cell(row=row, column=3), 'Points (incl. lock)')
        column = 4
        for stat in stat_list:
            ExcelUtility.set_cell(ws.cell(row=row, column=column), stat.points)
            column += 1

        # Set the column widths to the widest cell in each column
        for column in ws.columns:
            adjusted_width = max(len(ExcelUtility.as_text(cell.value)) for cell in column)
            cell = column[0]
            if cell.col_idx < 4:
                ws.column_dimensions[column[0].column_letter].width = adjusted_width
            else:
                ws.column_dimensions[column[0].column_letter].width = 9

    @staticmethod
    def as_text(value):
        if value is None:
            return ''
        return str(value)

    @staticmethod
    def set_cell(cell, value, font=FONT):
        cell.value = value
        cell.font = font

    @staticmethod
    def format_pick(pick):
        """Returns a string in the TEAM_1 @ TEAM_2, with the opening line beside the favorite team

        Here are examples of the string returned:

        - CLEM -12 @ UVA
        - CLEM @ UVA -12

        :param pick: the Pick to format
        :return: a string
        """
        if pick.team is None:
            return ''

        code = pick.team.code
        asterisk = ''
        if pick.lock:
            asterisk = '*'

        return '{}{}{}'.format(asterisk, code, asterisk)

    @staticmethod
    def format_game(matchup):
        """Returns a string in the format TEAM_1 @ TEAM_2, with the opening line beside the favorite team

        Here are examples of the string returned:

        - CLEM -12 @ UVA
        - CLEM @ UVA -12

        :param matchup: the Matchup to format
        :return: a string
        """
        if matchup.away_team == matchup.favorite:
            away_str = '{} -{}'.format(matchup.away_team.code, matchup.opening_line)
        else:
            away_str = '{}'.format(matchup.away_team.code)

        if matchup.home_team == matchup.favorite:
            home_str = '{} -{}'.format(matchup.home_team.code, matchup.opening_line)
        else:
            home_str = '{}'.format(matchup.home_team.code)

        return '{} @ {}'.format(away_str, home_str)

    @staticmethod
    def format_team(team, matchup):
        """Returns a string in the format TEAM (-OPENING_LINE).  E.g. CLEM 17 (-7)

        Here are examples of the string returned:

        - CLEM          # the game has not started and CLEM is not the favorite
        - CLEM (-7)     # the game has not started and CLEM is the favorite by 7 points
        - CLEM 17 (-7)  # the game is complete, CLEM is the favorite by 7 points, and CLEM scored 17 points
        - CLEM 17       # the game is complete, CLEM is not the favorite, and CLEM scored 17 points

        :param team: the Team to format
        :param matchup: the Matchup the team is a part of
        :return: a string
        """
        if team == matchup.favorite:
            spread = ' (-{})'.format(matchup.opening_line)
        else:
            spread = ''

        points = ''
        if matchup.is_complete():
            if team == matchup.home_team:
                points = ' {}'.format(matchup.home_score)
            else:
                points = ' {}'.format(matchup.away_score)

        return '{}{}{}'.format(team.code, points, spread)


class BackupRestore(object):
    """This creates an Excel workbook that has all the data in the database.

    It creates the following tabs:

    - Team
    - Matchup
    - Pick
    - WeeklyStat

    This is also the order in which the data should be loaded into the target system

    https://simpleisbetterthancomplex.com/tutorial/2016/07/29/how-to-export-to-excel.html
    https://djangotricks.blogspot.com/2019/02/how-to-export-data-to-xlsx-files.html
    https://dev.to/coderasha/how-to-export-import-data-django-package-series-3-39mk
    """

    CSV_TEAM = 'collegefbpool.com-backup-team.csv'
    CSV_MATCHUP = 'collegefbpool.com-backup-matchup.csv'
    CSV_PICK = 'collegefbpool.com-backup-pick.csv'
    CSV_WEEKLYSTAT = 'collegefbpool.com-backup-weeklystat.csv'
    XLSX = 'collegefbpool.com-backup.xlsx'

    def __init__(self):
        self.wb = None

    def backup(self):
        # book = Databook()
        # book.add_sheet(TeamResource().export())
        # book.add_sheet(MatchupResource().export())
        # book.add_sheet(PickResource().export())
        # book.add_sheet(WeeklyStatsResource().export())
        # with open(self.XLSX, 'wb') as f:
        #     f.write(book.export('xlsx'))

        data = TeamResource().export()
        with open(self.CSV_TEAM, 'w', newline='') as f:
            f.write(data.export('csv'))

        data = MatchupResource().export()
        with open(self.CSV_MATCHUP, 'w', newline='') as f:
            f.write(data.export('csv'))

        data = PickResource().export()
        with open(self.CSV_PICK, 'w', newline='') as f:
            f.write(data.export('csv'))

        data = WeeklyStatsResource().export()
        with open(self.CSV_WEEKLYSTAT, 'w', newline='') as f:
            f.write(data.export('csv'))

    def restore(self):
        """
        team_resource = TeamResource()
        team_data = Dataset().load(open(self.CSV_TEAM).read())
        team_result = team_resource.import_data(team_data, dry_run=True)
        if team_result.has_errors():
            return 'Error in Team dry run'
        """

        matchup_resource = MatchupResource()
        matchup_data = Dataset().load(open(self.CSV_MATCHUP).read())
        matchup_result = matchup_resource.import_data(matchup_data, dry_run=True)
        if matchup_result.has_errors():
            return 'Error in Matchup dry run'

        pick_resource = PickResource()
        pick_data = Dataset().load(open(self.CSV_PICK).read())
        pick_result = pick_resource.import_data(pick_data, dry_run=True)
        if pick_result.has_errors():
            return 'Error in Pick dry run'

        weekly_stats_resource = WeeklyStatsResource()
        weekly_stats_data = Dataset().load(open(self.CSV_WEEKLYSTAT).read())
        weekly_stats_result = weekly_stats_resource.import_data(weekly_stats_data, dry_run=True)
        if weekly_stats_result.has_errors():
            return 'Error in Weekly Stats dry run'

        # Team.objects.all().delete()
        # team_resource.import_data(team_data)
        Matchup.objects.all().delete()
        matchup_resource.import_data(matchup_data)
        Pick.objects.all().delete()
        pick_resource.import_data(pick_data)
        WeeklyStats.objects.all().delete()
        weekly_stats_resource.import_data(weekly_stats_data)

        return 'Success'

    def _restore(self, resource, csv):
        data = Dataset().load(open(csv).read())
        result = resource.import_data(data, dry_run=True)


"""
        self.wb = Workbook()
        self.wb.remove_sheet(self.wb.active)

        self.model_to_sheet('User', get_user_model(), ['logentry', 'profile', 'groups', 'user_permissions'])
        self.model_to_sheet('Profile', Profile)
        self.model_to_sheet('Team', Team)
        self.model_to_sheet('Matchup', Matchup, ['picks'])
        self.model_to_sheet('Pick', Pick)
        self.model_to_sheet('WeeklyStats', WeeklyStats)

        self.wb.save(self.WB_NAME)


    def model_to_sheet(self, model_name, model, skip_fields=[]):
        ws = self.wb.create_sheet(model_name)

        fields = model._meta.get_fields(include_parents=False)
        queryset = model.objects.all().order_by('id')

        field_names = []
        for field in fields:
            if field.name in skip_fields:
                continue
            field_names.append(field.name)

        for field_num, field_name in enumerate(field_names, 1):
            cell = ws.cell(row=1, column=field_num)
            cell.value = field_name

        row_num = 2
        for row in queryset:
            col_num = 1
            for field in field_names:
                value = getattr(row, field)
                cell = ws.cell(row=row_num, column=col_num)
                cell.value = '{}'.format(value)
                col_num += 1
            row_num += 1

    def restore(self):
        # https://django-import-export.readthedocs.io/en/latest/getting_started.html
        self.wb = load_workbook(self.WB_NAME)
        self.sheet_to_model('Team', Team)

    def sheet_to_model(self, model_name, model):
        ws = self.wb.get_sheet_by_name(model_name)
        print(ws)
"""
