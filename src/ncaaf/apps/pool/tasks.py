"""Provides functions that can be called from batch schedulers."""

from celery import shared_task
from django.utils import timezone
from .models import Config, Matchup, WeeklyStats
from ..stats.models import LiveWeek
from ..member.models import SMS
import logging


@shared_task
def update_matchups(now=None):
    """This task gets the latest information from ESPN for each matchup in the current week."""

    if not Config.run_batches():
        return

    def formatter(count, label): return '' if count == 0 else '{} {}.  '.format(count, label)

    live_week = LiveWeek(Config.get_week())
    complete, pregame, in_game, post_game, postponed = live_week.update_matchups(Config.get_year(), Config.get_week())

    result = 'Matchups updated: ' + formatter(complete, 'complete') + formatter(pregame, 'pregame') + \
        formatter(in_game, 'in game') + formatter(post_game, 'post game') + formatter(postponed, 'postponed')

    if post_game > 0:
        stats = WeeklyStats.calc_weekly_stats(Config.get_year(), Config.get_week())
        result += '  Calculated stats for {} users.'.format(stats)

    if now is None:
        now = timezone.now()

    sec = (now.minute * 60) + now.second
    if (0 <= sec < 120) & Config.send_heartbeat() & (9 <= now.hour <= 23):
        # Peace of mind once/hour knowing this is working properly.  Don't run overnight, though
        SMS.send_message(result, '7047377056')


@shared_task
def flip_matchups():
    """This task checks if any matchups that were open for picks need to be moved to locked."""

    if not Config.run_batches():
        return

    flipped = Matchup.move_to_locked(timezone.now(), Config.get_year(), Config.get_week())
    result = '{} matchups flipped to locked.'.format(flipped)
    if flipped > 0:
        # TODO: Send to commissioner rather than hard coding
        SMS.send_message(result, '7047377056')


@shared_task()
def notify_picks():
    """This task checks if anyone has not yet made a pick for a matchup and the matchup is about to move to locked."""

    logger = logging.getLogger('ncaaf')
    logger.info('start')
    picks = Matchup.notify_pick_needed(timezone.now(), Config.get_year(), Config.get_week())
    SMS.queue_message_to_all('Go to http://collegefbpool.com/pool/picks to make your pick(s)')
    count = SMS.send_queued_messages()
    logger.info('count: {}'.format(count))
    if count > 0:
        message = 'Sent notifications to {} users that a total of {} picks are needed'.format(count, picks)
        SMS.send_message(message, '7047377056')
    return count
