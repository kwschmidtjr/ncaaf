from datetime import datetime
import pytz

from django.test import TestCase

from ..tasks import update_matchups


class TaskTestCse(TestCase):

    def test_update_matchups_no_SMS_before(self):
        now = datetime(2020, 4, 26, 9, 59, 59, tzinfo=pytz.utc)
        update_matchups(now)

    def test_update_matchups(self):
        now = datetime(2020, 4, 26, 10, 1, 0, tzinfo=pytz.utc)
        update_matchups(now)

    def test_update_matchups_no_SMS_after(self):
        now = datetime(2020, 4, 26, 10, 2, 0, tzinfo=pytz.utc)
        update_matchups(now)

