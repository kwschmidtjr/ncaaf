from django.test import TestCase, Client
from django.urls import reverse
from .base_data import NcaafTestData, TEST_WEEK

"""
class HomeViewTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.url = reverse('home')

    # BUG This test fails because django appends a redirect parameter 
    # Response redirected to '/member/login/?next=/'
    # expected '/member/login/'Expected '/member/login/?next=/' to equal '/member/login/'.
    def test_home_view_redirects_unauthenticated_user_to_login(self):
        response = self.client.get(self.url)
        self.assertRedirects(response, reverse('login') + "?next=/")


class TeamViewTestCase(TestCase):

    def setUp(self):
        self.url = reverse('teams')

    def test_teams_returns_form_for_get(self):
        response = self.client.get(self.url)
        self.assertTemplateUsed('teams.html')


class MatchupViewTestCase(TestCase):

    data = NcaafTestData()

    def setUp(self):
        self.data.set_up()
        self.url = reverse('matchups')

    def test_matchup_gets_proper_week(self):
        proper_week = '<h3>Set Matchups for Week {}</h3>'.format(TEST_WEEK)
        self.client.force_login(self.data.user_1)
        response = self.client.get(self.url)
        self.assertTemplateUsed('matchups.html')
        self.assertInHTML(proper_week, response.content.decode())

    def test_matchup_gets_proper_number_of_matchups(self):
        # setUp creates two matchups.  This checks that the get method returns two forms
        self.client.force_login(self.data.user_1)
        response = self.client.get(self.url)
        mgt_form = response.context['formset'].management_form
        num_forms = mgt_form['TOTAL_FORMS'].value()
        self.assertEquals(2, num_forms)

    def test_matchup_post_plus_one(self):
        # setUp creates two matchups.  This checks that the get method returns two forms.
        self.client.force_login(self.data.user_1)
        response = self.client.get(self.url)
        mgt_form = response.context['formset'].management_form

"""

"""
    
    self.assertEqual(BlahForm, response.context['form'].__class__)
    
    
    
    for i in range(response.context['form'].total_form_count()):
    # get form index 'i'
    current_form = response.context['form'].forms[i]

    # retrieve all the fields
    for field_name in current_form.fields:
        value = current_form[field_name].value()
        data['%s-%s' % (current_form.prefix, field_name)] = value if value is not None else ''
"""
