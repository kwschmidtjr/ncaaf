from datetime import datetime
import pytz

from django.test import TestCase
from django.core.exceptions import ValidationError
from django.utils import timezone

from ..models import Pick, WeeklyStats, Stats, Matchup, TZ
from ...member.models import SMS
from .base_data import NcaafTestData, TEST_YEAR, TEST_WEEK

'''
The object NcaafTestData sets up a group of object relationships that is used extensively in these test cases.
Refer to the object diagram in the docs to get a visual of the relationships and attributes.
'''


class MatchupModelTestCase(TestCase):
    data = NcaafTestData()

    def setUp(self):
        self.data.set_up()

    def test_pick1a_winner(self):
        self.data.set_matchup_a()
        self.data.set_pick_1a()
        self.data.matchup_a.set_complete()
        test_winner = self.data.matchup_a.winner()
        self.assertEqual(test_winner, self.data.home_team_a)

    def test_pick1b_winner(self):
        self.data.set_matchup_b()
        self.data.set_pick_1b()
        self.data.matchup_b.set_complete()
        test_winner = self.data.matchup_b.winner()
        self.assertEqual(test_winner, self.data.away_team_b)

    def matchup_helper(self, away_score, home_score, opening_line, favorite, winner):
        self.data.matchup_a.away_score = away_score
        self.data.matchup_a.home_score = home_score
        self.data.matchup_a.favorite = favorite
        self.data.matchup_a.opening_line = opening_line
        self.data.matchup_a.set_complete()

        test_winner = self.data.matchup_a.winner()
        self.assertEqual(winner, test_winner)

    def test_no_winner(self):
        # Set as no score and confirm there is no winner
        test_winner = self.data.matchup_a.winner()
        self.assertEqual(None, test_winner)

    def test_home_favorite_covers(self):
        # Set home team as favorite, home team winning by more than spread, and check if it covers
        self.matchup_helper(0, 10, 8.5, self.data.home_team_a, self.data.home_team_a)

    def test_home_favorite_does_not_cover(self):
        # Set home team as favorite, home team winning by less than spread, and check that it does not cover
        self.matchup_helper(0, 10, 11.5, self.data.home_team_a, self.data.away_team_a)

    def test_away_favorite_covers(self):
        # Set away team as favorite, away team winning by more than spread, and check if it covers
        self.matchup_helper(10, 0, 8.5, self.data.away_team_a, self.data.away_team_a)

    def test_away_favorite_does_not_cover(self):
        # Set away team as favorite, away team winning by less than spread, and check that it does not cover
        self.matchup_helper(10, 0, 11.5, self.data.away_team_a, self.data.home_team_a)

    def test_push_away(self):
        # A win matching exactly the spread is a push.
        self.matchup_helper(10, 0, 10, self.data.away_team_a, None)

    def test_push_home(self):
        # A win matching exactly the spread is a push.
        self.matchup_helper(0, 10, 10, self.data.home_team_a, None)

    def test_move_to_locked(self):
        self.data.set_matchup_a()
        self.data.set_matchup_b()
        Matchup.move_to_locked(timezone.now(), self.data.config.get_year(), self.data.config.get_week())
        matchup_a = Matchup.objects.get(pk=self.data.matchup_a.pk)
        matchup_b = Matchup.objects.get(pk=self.data.matchup_b.pk)
        self.assertTrue(matchup_a.is_locked())
        # self.assertFalse(matchup_b.is_locked())

    def test_move_to_locked_sat_game_after_fri_5pm(self):
        fri = datetime(2020, 4, 24, 17, 1, 0, tzinfo=pytz.utc)  # Apr 24, 2020 (5:01pm ET) is a Friday
        fri -= TZ.utc_offset()
        sat = datetime(2020, 4, 25, 9, tzinfo=pytz.utc)  # Apr 25, 2020 is a Saturday
        # Using these steps to work around the state rules enforced by the matchup save method
        # self.data.matchup_a.set_open_for_setup()
        self.data.matchup_a.date_time_of_game = sat
        self.data.matchup_a.save()
        Matchup.move_to_locked(fri, self.data.config.get_year(), self.data.config.get_week())
        matchup_a = Matchup.objects.get(pk=self.data.matchup_a.pk)
        self.assertTrue(matchup_a.is_locked())

    def test_move_to_locked_thu_game_one_hour_ahead(self):
        thu_1 = datetime(2020, 4, 23, 18, 59, 0, tzinfo=pytz.utc)  # Apr 23, 2020 (6:59pm ET) is a Thursday
        thu_2 = datetime(2020, 4, 23, 19, 1, 0, tzinfo=pytz.utc)
        game_time = datetime(2020, 4, 23, 20, 0, 0, tzinfo=pytz.utc)
        self.data.matchup_a.date_time_of_game = game_time
        self.data.matchup_a.save()

        # At 61 minutes prior to game, should not move to locked
        Matchup.move_to_locked(thu_1, self.data.config.get_year(), self.data.config.get_week())
        matchup_a = Matchup.objects.get(pk=self.data.matchup_a.pk)
        self.assertFalse(matchup_a.is_locked())

        # At 59 minutes prior to game, should move to locked
        Matchup.move_to_locked(thu_2, self.data.config.get_year(), self.data.config.get_week())
        matchup_a = Matchup.objects.get(pk=self.data.matchup_a.pk)
        self.assertTrue(matchup_a.is_locked())

    def test_notify_pick_needed(self):
        dt = timezone.now()
        self.data.set_matchup_a(dt)
        self.data.set_matchup_b(dt)
        count = Matchup.notify_pick_needed(dt, self.data.config.get_year(), self.data.config.get_week())
        # self.assertEqual(count, 2)

    def test_notify_pick_needed_nearing_fri_5pm(self):
        self.data.user_1.profile.text_reminder = True
        self.data.user_1.profile.save()

        fri = datetime(2020, 4, 24, 15, 1, 0, tzinfo=pytz.utc)  # Apr 24, 2020 (3:01pm ET) is a Friday
        fri -= TZ.utc_offset()
        sat = datetime(2020, 4, 25, 16, 0, 0, tzinfo=pytz.utc)
        self.data.matchup_a.date_time_of_game = sat
        self.data.matchup_b.date_time_of_game = sat
        self.data.matchup_a.save()
        self.data.matchup_b.save()
        count = Matchup.notify_pick_needed(fri, self.data.config.get_year(), self.data.config.get_week())
        self.assertEqual(count, 2)
        SMS.queue_message_to_all('Go to http://collegefbpool.com/pool/picks to make your pick(s)')
        count = SMS.send_queued_messages(dry_run=True)
        self.assertEqual(count, 1)

    def test_notify_pick_not_needed_nearing_fri_5pm(self):
        self.data.user_1.profile.text_reminder = True
        self.data.user_1.profile.save()

        fri = datetime(2020, 4, 24, 14, 59, 0, tzinfo=pytz.utc)  # Apr 24, 2020 (2:59pm ET) is a Friday
        fri -= TZ.utc_offset()
        sat = datetime(2020, 4, 25, 14, 1, 0, tzinfo=pytz.utc)
        self.data.matchup_a.date_time_of_game = sat
        self.data.matchup_a.save()
        count = Matchup.notify_pick_needed(fri, self.data.config.get_year(), self.data.config.get_week())
        self.assertEqual(count, 0)
        count = SMS.send_queued_messages(dry_run=True)
        self.assertEqual(count, 0)

    def test_game_ready_matchups(self):
        self.data.set_matchup_a()
        self.data.set_matchup_b()
        matchup_list = Matchup.get_game_ready_matchups(TEST_YEAR, TEST_WEEK)
        self.assertEqual(0, matchup_list.count())

        self.data.matchup_a.set_locked()
        matchup_list = Matchup.get_game_ready_matchups(TEST_YEAR, TEST_WEEK)
        self.assertEqual(1, matchup_list.count())

    def test_get_pick_count(self):
        count = self.data.matchup_a.get_pick_count()
        self.assertEqual(0, count)

        self.data.set_pick_1a()
        count = self.data.matchup_a.get_pick_count()
        self.assertEqual(1, count)


class PickModelTestCase(TestCase):
    data = NcaafTestData()

    def setUp(self):
        # Added a test case that needed no matchups so moved the data set up to each test case
        # self.data.set_up()
        pass

    def test_points(self):
        self.data.set_up()

        # home team favorite, covers the spread, and is a lock
        self.data.set_matchup_a()
        self.data.set_pick_1a()
        self.data.matchup_a.set_complete()
        points = self.data.pick_1a.points()
        self.assertTrue(points == 3)

        # now make it not the lock
        self.data.pick_1a.lock = False
        points = self.data.pick_1a.points()
        self.assertTrue(points == 1)

        # now make home team not cover
        self.data.matchup_a.home_score = 7
        points = self.data.pick_1a.points()
        self.assertTrue(points == 0)

    def test_points_for_week(self):
        self.data.set_up()

        # sets up pick_1a as a win and lock -> 3 points
        self.data.set_matchup_a()
        self.data.set_pick_1a()
        self.data.matchup_a.set_complete()

        # sets up pick_1b as a loss -> 0 points
        self.data.set_matchup_b()
        self.data.set_pick_1b()
        self.data.matchup_b.set_complete()

        test_points = Pick.points_for_week(self.data.user_1, TEST_YEAR, TEST_WEEK)
        self.assertEqual(test_points, 3)

    def test_get_picks_for_week_no_matchups(self):
        self.data.set_up(set_matchups=False)
        pick_list = Pick.get_picks_for_week(self.data.user_1, TEST_YEAR, TEST_WEEK)
        self.assertEqual(pick_list.count(), 0)

    def test_get_picks_for_week(self):
        self.data.set_up()

        # initially there should be two picks for this user
        pick_list = Pick.get_picks_for_week(self.data.user_1, TEST_YEAR, TEST_WEEK)
        self.assertEqual(pick_list.count(), 2)

        # after adding another matchup, there should be three picks for this user
        matchup_c = self.data.set_matchup(self.data.away_team_a, self.data.home_team_b, self.data.away_team_a)
        pick_list = Pick.get_picks_for_week(self.data.user_1, TEST_YEAR, TEST_WEEK)
        self.assertEqual(pick_list.count(), 3)

    def test_get_teams_for_pick(self):
        self.data.set_up()

        team_list = self.data.pick_1a.get_teams_for_pick()
        self.assertEqual(team_list.count(), 2)

        self.assertEqual(self.data.away_team_a, team_list.first())
        self.assertEqual(self.data.home_team_a, team_list.last())

    def test_state_changes(self):
        # The ability to save or create a Pick is subject to the Matchup to which the Pick is associated.
        # A pick can only be created or saved when the Matchup is in state PICKS or SETUP.  All other attempts to
        # create or save a Pick will create the same ValidationError, so only need to test one of those states.
        self.data.set_up(set_matchups=False)
        matchup = self.data.set_matchup(self.data.away_team_a, self.data.home_team_a, self.data.home_team_a)
        matchup.state = Matchup.LOCKED
        matchup.save()
        try:
            pick_1 = Pick.objects.create(user=self.data.user_1, matchup=matchup)
        except ValidationError as error:
            # When Matchup is in SETUP state, picks cannot be created or saved
            self.assertEqual(error.code, 'STATE_VIOLATION')

        matchup.set_open_for_picks()
        pick_1 = Pick.objects.create(user=self.data.user_1, matchup=matchup)
        pick = Pick.objects.filter(user=self.data.user_1, matchup=matchup).first()
        # self.assertEqual(pick, pick_1)  # Confirm the pick saved by checking what came back from the DB

    def test_make_empty_picks(self):
        self.data.set_up(set_matchups=True, set_picks=False)

        pick_list = Pick.objects.all()
        self.assertEqual(pick_list.count(), 0)

        Pick.make_empty_picks(TEST_YEAR, TEST_WEEK)
        pick_list = Pick.objects.all()
        self.assertEqual(pick_list.count(), 4)

    def test_auto_pick_no_pick(self):
        self.data.set_up()
        self.data.profile_1.auto_pick = True
        self.data.profile_1.save()
        Pick.auto_pick(self.data.matchup_a)

        # Refresh the pick objects from the database
        pick_1a = Pick.objects.get(pk=self.data.pick_1a.pk)
        pick_2a = Pick.objects.get(pk=self.data.pick_2a.pk)
        self.assertIn(pick_1a.team, [self.data.matchup_a.away_team, self.data.matchup_a.home_team])
        self.assertEqual(pick_2a.team, None)

    def test_auto_pick_with_pick(self):
        # Also write one to figure out why dumb ass is not doing auto pick
        self.data.set_up()
        self.data.set_matchup_a()
        self.data.set_pick_1a()
        self.data.profile_1.auto_pick = True
        self.data.profile_1.save()

        # This is a bit of a workaround.  The auto_pick method will randomly pick one of the two teams.  Simply running
        # one time will work 50% of the time by chance.  By running 10 times, this chance is reduced to 0.01%
        for i in range(10):
            Pick.auto_pick(self.data.matchup_a)

            # Refresh the pick objects from the database
            pick_1a = Pick.objects.get(pk=self.data.pick_1a.pk)
            self.assertEqual(pick_1a.team, self.data.matchup_a.home_team)


class WeeklyStatsTestCase(TestCase):
    data = NcaafTestData()

    def setUp(self):
        self.data.set_up()

    def test_calc_weekly_stats(self):
        # there are two users so the result should be two objects in WeeklyStats
        count = WeeklyStats.calc_weekly_stats(TEST_YEAR, TEST_WEEK)
        self.assertEqual(count, 2)
        stats_list = WeeklyStats.objects.filter(year=TEST_YEAR, week=TEST_WEEK)
        self.assertEqual(stats_list.count(), 2)

        # check that user_1 has no points
        stats_list = WeeklyStats.objects.filter(user=self.data.user_1, year=TEST_YEAR, week=TEST_WEEK)
        self.assertEqual(stats_list.first().points, 0)

        # set pick_1a to be worth 3 points and verify the WeeklyStats object is still zero because
        # the matchup is in PICKS status
        self.data.set_matchup_a()
        self.data.set_pick_1a()
        WeeklyStats.calc_weekly_stats(TEST_YEAR, TEST_WEEK)
        stats_list = WeeklyStats.objects.filter(user=self.data.user_1, year=TEST_YEAR, week=TEST_WEEK)
        stat = stats_list.first()
        self.assertEqual(stat.points, 0)

        # once the matchup is in COMPLETE status, the points should total 3
        self.data.matchup_a.set_complete()
        WeeklyStats.calc_weekly_stats(TEST_YEAR, TEST_WEEK)
        stats_list = WeeklyStats.objects.filter(user=self.data.user_1, year=TEST_YEAR, week=TEST_WEEK)
        stat = stats_list.first()
        self.assertEqual(stat.points, 3)


class StatsTestCase(TestCase):
    data = NcaafTestData()

    def setUp(self):
        self.data.set_up()

    def test_sum_points(self):
        self.data.set_matchup_a()
        self.data.set_pick_1a()
        self.data.matchup_a.set_complete()
        WeeklyStats.calc_weekly_stats(TEST_YEAR, TEST_WEEK)
        stats = Stats(self.data.user_1, TEST_YEAR)
        points = stats.total_points()
        self.assertEqual(points, 3)

    def test_sum_locks(self):
        self.data.set_matchup_a()
        self.data.set_pick_1a()
        self.data.matchup_a.set_complete()
        WeeklyStats.calc_weekly_stats(TEST_YEAR, TEST_WEEK)
        stats = Stats(self.data.user_1, TEST_YEAR)
        locks = stats.total_locks()
        self.assertEqual(locks, 1)

    def test_points_and_locks_lead(self):
        self.data.set_matchup_a()
        self.data.set_matchup_b()
        self.data.matchup_a.set_open_for_picks()
        self.data.matchup_b.set_open_for_picks()
        self.data.set_pick_1a()  # two points for user_a
        self.data.set_pick_1b()  # zero points for user_b
        # points only calculated for complete matchups
        self.data.matchup_a.set_complete()
        self.data.matchup_b.set_complete()
        WeeklyStats.calc_weekly_stats(TEST_YEAR, TEST_WEEK)
        stats_list = Stats.all(TEST_YEAR)
        self.assertTrue(stats_list[0].most_points)
        self.assertTrue(stats_list[0].most_locks)
        self.assertFalse(stats_list[1].most_points)
        self.assertFalse(stats_list[1].most_locks)


class TestTZ(TestCase):

    # Offsets from UTC for eastern daylight and eastern standard
    EDT_FROM_UTC = -4
    EST_FROM_UTC = -5
    MINS_PER_HOUR = 60 * 60

    def test_next_sat_noon_from_sun(self):
        sun = datetime(2020, 4, 26, 9, tzinfo=pytz.utc)  # Apr 26, 2020 is a Sunday, so May 2 is next Saturday
        next_sat = TZ.next_sat_noon(sun)
        self.assertEqual(next_sat.weekday(), 5)
        self.assertEqual(next_sat.month, 5)
        self.assertEqual(next_sat.day, 2)

    def test_next_sat_noon_from_sat(self):
        sat = datetime(2020, 4, 25, 9, tzinfo=pytz.utc)  # Apr 25, 2020 is a Saturday, so May 2 is next Saturday
        next_sat = TZ.next_sat_noon(sat)
        self.assertEqual(next_sat.weekday(), 5)
        self.assertEqual(next_sat.month, 5)
        self.assertEqual(next_sat.day, 2)

    def test_next_sat_noon_from_fri(self):
        fri = datetime(2020, 4, 24, 9, tzinfo=pytz.utc)  # Apr 24, 2020 is a Friday, so Apr 25 is next Saturday
        next_sat = TZ.next_sat_noon(fri)
        self.assertEqual(next_sat.weekday(), 5)
        self.assertEqual(next_sat.month, 4)
        self.assertEqual(next_sat.day, 25)

    def test_weekly_cutoff_time_from_sun(self):
        # The weekly cutoff time for picks is Friday 5pm ET and maintained in the constants TZ.CUTOFF_DAY
        # and TZ.CUTOFF_TIME.
        sun = datetime(2020, 4, 26, 9, tzinfo=pytz.utc)  # Apr 26, 2020 is a Sunday, so May 1 is next Friday
        correct_cutoff = datetime(2020, 5, 1, 17, tzinfo=pytz.utc)
        correct_cutoff -= TZ.utc_offset()
        cutoff_time = TZ.weekly_cutoff_time(sun)
        self.assertEqual(correct_cutoff, cutoff_time)

    def test_weekly_cutoff_time_from_fri_early(self):
        # The weekly cutoff time for picks is Friday 5pm ET and maintained in the constants TZ.CUTOFF_DAY
        # and TZ.CUTOFF_TIME.
        fri = datetime(2020, 4, 24, 16, 59, 0, tzinfo=pytz.utc)  # Apr 24, 2020 (4:59pmm) is a Friday
        fri -= TZ.utc_offset()
        correct_cutoff = datetime(2020, 4, 24, 17, tzinfo=pytz.utc)
        correct_cutoff -= TZ.utc_offset()
        cutoff_time = TZ.weekly_cutoff_time(fri)
        self.assertEqual(correct_cutoff, cutoff_time)

    def test_weekly_cutoff_time_from_fri_late(self):
        # The weekly cutoff time for picks is Friday 5pm ET and maintained in the constants TZ.CUTOFF_DAY
        # and TZ.CUTOFF_TIME.
        fri = datetime(2020, 4, 24, 17, 1, 0, tzinfo=pytz.utc)  # Apr 24, 2020 (5:01pmm) is a Friday
        fri -= TZ.utc_offset()
        correct_cutoff = datetime(2020, 4, 24, 17, tzinfo=pytz.utc)
        correct_cutoff -= TZ.utc_offset()
        cutoff_time = TZ.weekly_cutoff_time(fri)
        self.assertEqual(correct_cutoff, cutoff_time)

    def test_weekly_cutoff_time_from_sat(self):
        # The weekly cutoff time for picks is Friday 5pm ET and maintained in the constants TZ.CUTOFF_DAY
        # and TZ.CUTOFF_TIME.
        fri = datetime(2020, 4, 25, tzinfo=pytz.utc)  # Apr 25, 2020 is a Saturday
        fri -= TZ.utc_offset()
        correct_cutoff = datetime(2020, 4, 24, 17, tzinfo=pytz.utc)
        correct_cutoff -= TZ.utc_offset()
        cutoff_time = TZ.weekly_cutoff_time(fri)
        self.assertEqual(correct_cutoff, cutoff_time)

    def test_initializes_to_utc(self):
        et = TZ()
        next_sat = et.next_sat_noon()
        self.assertEqual(next_sat.tzinfo, pytz.utc)

    def test_utc_offset_edt(self):
        edt = datetime(2020, 4, 15, tzinfo=pytz.utc)
        offset = TZ.utc_offset(edt)
        offset_hours = offset.total_seconds() / self.MINS_PER_HOUR
        self.assertEqual(offset_hours, self.EDT_FROM_UTC)

    def test_utc_offset_est(self):
        est = datetime(2020, 2, 15, tzinfo=pytz.utc)
        offset = TZ.utc_offset(est)
        offset_hours = offset.total_seconds() / self.MINS_PER_HOUR
        self.assertEqual(offset_hours, self.EST_FROM_UTC)

