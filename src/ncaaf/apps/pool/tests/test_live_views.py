"""
from django.test import LiveServerTestCase
from django.urls import reverse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

# from django.contrib.auth.models import User
# BUG Need to user the Abstracted User model
from ncaaf.apps.member.models import User

import socket
import time

from .base_data import NcaafTestData, USER_1_PASSWORD

from django.test import override_settings
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


# https://selenium-python.readthedocs.io/locating-elements.html
# XPath Helper install on Chrome
@override_settings(ALLOWED_HOSTS=['*'])  # Disable ALLOW_HOSTS
class LiveTestCase(LiveServerTestCase):
    data = NcaafTestData()
    host = '0.0.0.0'

    @classmethod
    def setUpClass(cls):
        super(LiveTestCase, cls).setUpClass()
        # cls.browser = webdriver.Chrome()
        # cls.browser.implicitly_wait(10)
        cls.host = socket.gethostbyname(socket.gethostname())
        cls.browser = webdriver.Remote(
            command_executor='http://selenium:4444/wd/hub',
            desired_capabilities=DesiredCapabilities.CHROME,
        )
        cls.browser.implicitly_wait(10)

    @classmethod
    def tearDownClass(cls):
        # time.sleep(3)
        cls.browser.quit()
        super(LiveTestCase, cls).tearDownClass()

    def setUp(self):
        self.data.set_up()

    def login(self):
        self.browser.get(self.live_server_url)
        users = User.objects.all()
        user_1 = users.first()

        username = self.browser.find_element_by_id('id_username')
        password = self.browser.find_element_by_id('id_password')
        # both of these methods to get the button work
        # submit = self.selenium.find_element_by_tag_name('button')
        submit = self.browser.find_element_by_css_selector('button[type="submit"]')
        username.send_keys(user_1.username)
        password.send_keys(USER_1_PASSWORD)
        submit.send_keys(Keys.RETURN)  # here

    def nav_to_page(self, page):
        # Another way to navigate would be to get the URL like this:
        # week_url = self.live_server_url + reverse('week')
        # this would not provide a test of the navigation, of course, but would get to another page
        if page == 'week':
            link = self.browser.find_element_by_xpath("//li[@class='nav-item'][1]/a[@class='nav-link']")
        elif page == 'picks':
            link = self.browser.find_element_by_xpath("//li[@class='nav-item'][2]/a[@class='nav-link']")
        elif page == 'leaderboard':
            link = self.browser.find_element_by_xpath("//li[@class='nav-item'][3]/a[@class='nav-link']")
        elif page == 'matchups':
            link = self.browser.find_element_by_id('navbarDropdown')
            link.click()
            link = self.browser.find_element_by_xpath("//a[@class='dropdown-item'][2]")
        elif page == 'scores':
            link = self.browser.find_element_by_id('navbarDropdown')
            link.click()
            link = self.browser.find_element_by_xpath("//a[@class='dropdown-item'][3]")
        link.click()  # here

        # https://stackoverflow.com/questions/40998945/how-to-avoid-getting-nonetype-object-has-no-attribute-path-on-selenium-qui
        # https://stackoverflow.com/questions/38574821/exception-attributeerror-nonetype-object-has-no-attribute-path-in

    def test_login_ok(self):
        self.login()
        self.assertTrue(1 == 1)
    # def test_set_matchups(self):
    #     self.login()
    #     self.nav_to_page('matchups')
    #     save_button = self.browser.find_element_by_name('add_more')
    #     save_button.send_keys(Keys.RETURN)
    #     # save_button.click()

    #     matchup_c_away_team_dropdown = Select(self.browser.find_element_by_id('id_form-2-away_team'))
    #     matchup_c_away_team_dropdown.select_by_visible_text('AFA')

    #     matchup_c_home_team_dropdown = Select(self.browser.find_element_by_id('id_form-2-home_team'))
    #     matchup_c_home_team_dropdown.select_by_visible_text('ARK')

    #     matchup_c_favorite_dropdown = Select(self.browser.find_element_by_id('id_form-2-favorite'))
    #     matchup_c_favorite_dropdown.select_by_visible_text('ARK')

    #     matchup_c_opening_line = self.browser.find_element_by_id('id_form-2-opening_line')
    #     matchup_c_opening_line.send_keys(10)

    #     save_button = self.browser.find_element_by_name('save')
    #     save_button.click()

    #     message = self.browser.find_element_by_class_name('alert').text
    #     found = message.find('Your picks are saved!')
    #     self.assertTrue(found >= 0)

    # def test_make_picks(self):
    #     self.login()
    #     self.nav_to_page('picks')

    #     pick_1_dropdown = Select(self.browser.find_element_by_id('id_form-0-team'))
    #     pick_1_dropdown.select_by_visible_text('away_team_a')
    #     pick_1_lock = self.browser.find_element_by_id('id_form-0-lock')
    #     pick_1_lock.click()

    #     pick_2_dropdown = Select(self.browser.find_element_by_id('id_form-1-team'))
    #     pick_2_dropdown.select_by_visible_text('away_team_b')

    #     save_button = self.browser.find_element_by_name('save')
    #     save_button.click()  # here

    #     message = self.browser.find_element_by_class_name('alert').text
    #     found = message.find('Your picks are saved!')
    #     self.assertTrue(found >= 0)
    #     """
    #     /html/body/section[@class='container m-1']/div[@class='container']
    #     /form/div[@class='alert alert-success alert-dismissible fade show']
    #     """

    # def test_enter_scores(self):
    #     self.login()
    #     self.nav_to_page('scores')
    #     matchup_a_away_score_field = self.browser.find_element_by_id('id_form-0-away_score')
    #     matchup_a_home_score_field = self.browser.find_element_by_id('id_form-0-home_score')
    #     matchup_b_away_score_field = self.browser.find_element_by_id('id_form-1-away_score')
    #     matchup_b_home_score_field = self.browser.find_element_by_id('id_form-1-home_score')
    #     # TODO need to erase the number in the box before entering the new numbers
    #     # For example, send_keys('10') results in adding 10 in front of the 0 on the form,
    #     # resulting in 100 being entered
    #     matchup_a_away_score_field.send_keys('0')
    #     matchup_a_home_score_field.send_keys('10')
    #     matchup_b_away_score_field.send_keys('0')
    #     matchup_b_home_score_field.send_keys('10')
    #     save_button = self.browser.find_element_by_name('save')
    #     save_button.click()  # here

    #     message = self.browser.find_element_by_class_name('alert').text
    #     found = message.find('The scores are saved!')
    #     self.assertTrue(found >= 0)

#######

"""
def test_login(self):
    from selenium.webdriver.support.wait import WebDriverWait
    timeout = 2
    ...
    self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()
    # Wait until the response is received
    WebDriverWait(self.selenium, timeout).until(
        lambda driver: driver.find_element_by_tag_name('body'))
"""
