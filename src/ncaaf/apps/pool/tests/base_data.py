from django.contrib.auth import get_user_model
from django.utils import timezone

from ..models import Config, Matchup, Team, Pick, TZ
from ...member.models import Profile

TEST_YEAR = 2010
TEST_WEEK = 1
USER_1_PASSWORD = 'pass_1'
USER_2_PASSWORD = 'pass_2'


class NcaafTestData(object):

    def __init__(self) -> object:
        self.user_1 = None
        self.user_2 = None
        self.profile_1 = None
        self.profile_2 = None
        self.config = None
        self.away_team_a = None
        self.home_team_a = None
        self.away_team_b = None
        self.home_team_b = None
        self.matchup_a = None
        self.matchup_b = None
        self.pick_1a = None
        self.pick_1b = None
        self.pick_2a = None
        self.pick_2b = None

    def set_up(self, set_matchups=True, set_picks=True):
        """ Creates an integrated set of objects that can be used for multiple tests

        :param boolean set_matchups: determines if Matchups and their corresponding Picks
            are added (the default is True)
        :param boolean set_picks: determines if the Picks are set up for the Matchups (default is True).
            Note that if set_matchups is False, this parameter will be ignored.  Picks can not be set up
            if there are no Matchups

        Objects and relationships created:

        - away_team_a, home_team_a, away_team_b, home_team_b
        - matchup_a: away_team_a @ home_team_a (matchup.favorite=home_team_a)
        - matchup_b: away_team_b @ home_team_b (matchup.favorite=away_team_b)
        - user_1 -> pick_1a -> matchup_a (pick.team=None)
        - user_1 -> pick_1b -> matchup_b (pick.team=None)
        - user_2 -> pick_2a -> matchup_a (pick.team=None)
        - user_2 -> pick_2b -> matchup_b (pick.team=None)
        - user_1 -> profile_1
        - user_2 -> profile_2
        """
        user_model = get_user_model()
        self.user_1 = user_model.objects.create_superuser(
            username='user_1',
            first_name='user_1',
            password=USER_1_PASSWORD)
        self.user_2 = user_model.objects.create_superuser(
            username='user_2',
            first_name='user_2',
            password=USER_2_PASSWORD)

        self.profile_1 = Profile.objects.create(user=self.user_1, text_reminder=False, mobile_phone='7047377056')
        self.profile_2 = Profile.objects.create(user=self.user_2, text_reminder=False, mobile_phone='7047377056')

        # BUG away_team_a exceeded char limit of 5
        self.config = Config.objects.create(year=TEST_YEAR, week=TEST_WEEK)
        self.away_team_a = Team.objects.create(name='away_team_a', code='TGRS')
        self.home_team_a = Team.objects.create(name='home_team_a', code='DOGS')
        self.away_team_b = Team.objects.create(name='away_team_b', code='DOGS')
        self.home_team_b = Team.objects.create(name='home_team_b', code='BAMA')
        if set_matchups:
            self.matchup_a = self.set_matchup(self.away_team_a, self.home_team_a, self.home_team_a)
            self.matchup_b = self.set_matchup(self.away_team_b, self.home_team_b, self.home_team_b)
            self.matchup_a.save()
            self.matchup_b.save()
            if set_picks:
                self.pick_1a = self.set_pick(self.user_1, self.matchup_a)
                self.pick_1b = self.set_pick(self.user_1, self.matchup_b)
                self.pick_2a = self.set_pick(self.user_2, self.matchup_a)
                self.pick_2b = self.set_pick(self.user_2, self.matchup_b)

    def set_matchup(self, away_team, home_team, favorite):
        matchup = Matchup.objects.create(
            year=self.config.year,
            week=self.config.week,
            away_team=away_team,
            home_team=home_team,
            away_score=0,
            home_score=0,
            opening_line=0,
            favorite=favorite,
        )
        matchup.set_open_for_picks()
        return matchup

    def set_pick(self, user, matchup):
        pick = Pick.objects.create(
            user=user,
            matchup=matchup,
            team=None,
            lock=False,
            win=False)
        return pick

    def set_matchup_a(self, dt=timezone.now()):
        """Sets matchup_a with home_team_a as favorite and home_team_a covering the spread.

        This means that home_team_a is the winner.
        """
        self.matchup_a.state = Matchup.PICKS
        self.matchup_a.favorite = self.home_team_a
        self.matchup_a.home_score = 10
        self.matchup_a.away_score = 0
        self.matchup_a.opening_line = 7.5
        self.matchup_a.date_time_of_game = dt
        self.matchup_a.save()
        self.matchup_a.set_open_for_picks()

    def set_matchup_b(self, dt=timezone.now()):
        """Sets matchup_b with home_team_b as favorite and home_team_b does not cover the spread.

        This means that away_team_b is the winner.
        """
        self.matchup_b.state = Matchup.PICKS
        self.matchup_b.favorite = self.home_team_b
        self.matchup_b.home_score = 10
        self.matchup_b.away_score = 0
        self.matchup_b.opening_line = 11
        self.matchup_b.date_time_of_game = dt + TZ.HOUR + TZ.HOUR
        self.matchup_b.save()
        self.matchup_b.set_open_for_picks()

    def set_pick_1a(self):
        """Sets pick_1a to be a lock (2 points) and chooses home_team_a as winner.

        When this method is used alongside set_matchup_a, then pick_1a will be a winning pick (2 points).
        :rtype: object
        """
        self.pick_1a.team = self.home_team_a
        self.pick_1a.lock = True
        self.pick_1a.save()

    def set_pick_1b(self):
        """Sets pick_1b as not a lock (1 point) and chooses home_team_b as winner.

        When this method is used alongside set_matchup_b, then pick_1b will be a losing pick (0 points).
        """
        self.pick_1b.team = self.home_team_b
        self.pick_1b.lock = False
        self.pick_1b.save()
