from django.test import TestCase

from ..utils import ExcelUtility, BackupRestore
from ..models import Matchup
from .base_data import NcaafTestData


class ExcelUtilityTestCase(TestCase):

    data = NcaafTestData()

    def setUp(self):
        self.data.set_up()
        self.data.set_matchup_a()
        self.data.set_matchup_b()
        self.data.set_pick_1a()
        self.data.set_pick_1b()
        self.data.matchup_a.state = Matchup.COMPLETE
        self.data.matchup_a.save()
        self.data.matchup_b.state = Matchup.COMPLETE
        self.data.matchup_b.save()

    def test_create_workbook(self):
        ExcelUtility.create_workbook()
        self.assertEqual(True, True)


class ExcelBackupTestCase(TestCase):

    def setUp(self):
        pass

    def test_backup(self):
        # b = BackupRestore()
        # b.backup()
        self.assertEqual(True, True)