from django.conf.urls import url

from .views import batch_runner

urlpatterns = [
    url(r'batch_runner', batch_runner, name='batch_runner'),
]
