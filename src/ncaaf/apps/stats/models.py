"""Provides an interface to get team and matchup information from external APIs.

The current implementation is to use ESPN, but that could change in the future.
"""
import urllib3
import json
import logging

from datetime import timedelta, datetime
import pytz
from ..pool.models import Team, Matchup, Config, TZ, WeeklyStats

logger = logging.getLogger(__name__)


class LiveWeek(object):
    """Use this object to get the matchups for a week of the season."""
    _WEEK_URL = 'https://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard?week='
    # Ex https://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard?week=1
    # Ex https://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard?dates=20191125-20191130
    # Ex https://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard?dates=20200829-20200907
    # Ex https://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard?dates=20191130&limit=200

    _ESPN_ID = "matchup['id']"
    _CO1_NAME = "matchup['competitions'][0]['competitors'][0]['team']['displayName']"
    _CO2_NAME = "matchup['competitions'][0]['competitors'][1]['team']['displayName']"
    _DATE = "matchup['competitions'][0]['date']"
    _TEAMS = "matchup['competitions'][0]['competitors']"
    _ODDS_SPREAD = "matchup['competitions'][0]['odds'][0]['details']"
    _ODDS_OVER_UNDER = "matchup['competitions'][0]['odds'][0]['overUnder']"

    def __init__(self, week, http=urllib3.PoolManager(), matchup_json=None):
        self.http = http
        if matchup_json is None:
            # Use the ESPN URL to get the object
            url = '{}{}{}'.format(self._WEEK_URL, week, '&limit=200')
            response = self.http.request('GET', url)
            self.info = json.loads(response.data.decode('utf-8'))
        else:
            # Use the supplied json - for testing purposes
            self.info = matchup_json

    def load_matchup(self, espn_id):
        """Loads the matchup info from ESPN for the specified espn_id

        :param int espn_id: the ID from ESPN for the matchup to load.
        """
        live_matchup = LiveMatchup(espn_id, self.http)
        added, skipped = self._load_matchup(live_matchup.info)
        return added, skipped

    def load_matchups(self):
        """Get all the matchups for the week specified from ESPN and loads into the Matchup model.

        As this method loops through all the matchups from ESPN, it does two checks:
        * If one of the teams from ESPN doesn't exist in the Team model, the matchup will be skipped.
        * If the matchup has already been added to the Matchup model, the matchup will be skipped.

        :return: int matchups_added: a count of the number of matchups added
        :return: list matchups_skipped: a list of the matchups skipped
        """
        matchups_added = 0
        matchups_skipped = []
        for matchup in self.info['events']:
            added, skipped = self._load_matchup(matchup)
            matchups_added += added
            if added == 0:
                matchups_skipped.append(skipped)
        return matchups_added, matchups_skipped

    def _load_matchup(self, matchup):
        matchup_id = matchup['id']
        matchup_dt_string = eval(self._DATE)
        matchup_away_id, matchup_home_id = LiveWeek._get_away_home_teams(eval(self._TEAMS))

        # https://www.programiz.com/python-programming/datetime/strptime
        # Sample 2020-09-03T04:00Z
        dt = datetime.strptime(matchup_dt_string, '%Y-%m-%dT%H:%MZ')

        matchup_dt = datetime(dt.year, dt.month, dt.day, dt.hour, dt.minute, tzinfo=pytz.utc)

        # print('id: {}, date: {}, away: {}, home {}'.format(
        #    matchup_id, matchup_dt, matchup_away_id, matchup_home_id))

        away_team_list = Team.objects.filter(espn_id=matchup_away_id)
        home_team_list = Team.objects.filter(espn_id=matchup_home_id)

        if not away_team_list.exists() or not home_team_list.exists():
            # ESPN returns all matchups, even for teams from small schools.  Only create Matchup objects for the
            # ones where Matchup objects exist in the system.
            team_1 = eval(self._CO1_NAME)
            team_2 = eval(self._CO2_NAME)
            matchup_str = '{}   |   {}'.format(team_1, team_2)
            return 0, '{} - skipped because Team missing in DB'.format(matchup_str)

        matchup_list = Matchup.objects.filter(
            year=Config.get_year(),
            week=Config.get_week(),
            away_team=away_team_list.first(),
            home_team=home_team_list.first()
        )

        if matchup_list.count() == 1:
            # This will occur if the matchups for the week have already been loaded once
            return 0, '{} - skipped because matchup exists in DB'.format(matchup_list.first())

        matchup = Matchup.objects.create(
            year=Config.get_year(),
            week=Config.get_week(),
            date_time_of_game=matchup_dt,
            away_team=away_team_list.first(),
            home_team=home_team_list.first(),
            away_score=0,
            home_score=0,
            opening_line=0,
            favorite=None,
            state=Matchup.EXCLUDED,
            espn_id=matchup_id
        )
        return 1, '{} - added to DB'.format(matchup)

    @staticmethod
    def _get_away_home_teams(competitors):
        if competitors[0]['homeAway'] == 'home':
            home = competitors[0]['id']
            away = competitors[1]['id']
        else:
            home = competitors[1]['id']
            away = competitors[0]['id']
        return away, home

    def update_matchups(self, year, week):
        """Updates the scores and game info for matchups.

        Game Info is different data depending on the state of the Matchup.
        * Matchup not Started - day and time of game
        * Matchup in Progress - quarter game is in and time on the click.  E.g. 4th Quarter, 12:34
        * Matchup Complete - final

        :param int year: the week for which to update matchups
        :param int week: the week for which to update matchups
        :return: int a count of the number of matchups already complete before this method called
        :return: int a count of the number of pregame matchups updated
        :return: int a count of the number of in game matchups updated
        :return: int a count of the number of post game matchups updated
        :return: int a count of the number of matchups that were postponed
        """
        matchup_list = Matchup.objects.filter(year=year, week=week)
        complete_count = 0
        pregame_count = 0
        being_played_count = 0
        post_game_count = 0
        postponed_count = 0
        for matchup in matchup_list:
            if matchup.is_complete():
                complete_count += 1
                # Decided not to continue here.  This enables the processing to correct errors that may have
                # previously occurred.
                # continue  # No need to make further updates for already completed games
            live_matchup = LiveMatchup(matchup.espn_id, self.http)
            if live_matchup.is_pregame():
                pregame_count += 1
                dt = matchup.date_time_of_game
                dt += TZ.utc_offset()
                game_info = datetime.strftime(dt, '%a %I:%M %p')
            elif live_matchup.is_being_played():
                being_played_count += 1
                matchup.away_score = live_matchup.away_score()
                matchup.home_score = live_matchup.home_score()
                game_info = 'Q{} {}'.format(live_matchup.period(), live_matchup.clock())
            elif live_matchup.is_complete():
                post_game_count += 1
                matchup.away_score = live_matchup.away_score()
                matchup.home_score = live_matchup.home_score()
                game_info = 'Final'
                matchup.set_complete()
            elif live_matchup.is_cancelled():
                game_info = 'Cancelled'
            elif live_matchup.is_postponed():
                postponed_count += 1
                game_info = 'Postponed'
            else:
                game_info = 'Other'
            matchup.game_info = game_info
            matchup.current_spread = live_matchup.spread(Matchup._meta.get_field('current_spread').max_length)
            matchup.over_under = live_matchup.over_under()
            matchup.weather = live_matchup.weather(Matchup._meta.get_field('weather').max_length)
            matchup.high_temp = live_matchup.high_temp()
            matchup.save()

        return complete_count, pregame_count, being_played_count, post_game_count, postponed_count

    def update_odds(self):
        for matchup in self.info['events']:
            # Odds and over under are not always available in the ESPN API so will get KeyErrors
            try:
                spread = eval(self._ODDS_SPREAD)
            except KeyError:
                spread = ''

            try:
                over_under = eval(self._ODDS_OVER_UNDER)
            except KeyError:
                over_under = 0

            espn_id = matchup['id']
            matchup_list = Matchup.objects.filter(espn_id=espn_id)
            if matchup_list.count() == 1:
                m = matchup_list.first()
                max_length = Matchup._meta.get_field('current_spread').max_length
                spread = spread[0:max_length]
                m.current_spread = spread
                m.save()


class LiveMatchup(object):
    """Use this to get the live scores and other information for a matchup"""

    _SCOREBOARD_URL = 'https://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard/'

    # This example is the Clemson at South Carolina game on Nov 30, 2019
    # Example: https://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard/401110870
    # This example is the Pitt at Clemson game on Nov 28, 2020
    # Example: https://site.api.espn.com/apis/site/v2/sports/football/college-football/scoreboard/401234651

    # JSON paths
    _GAME_STATE = "self.info['competitions'][0]['status']['type']['state']"
    _GAME_STATE_NAME = "self.info['competitions'][0]['status']['type']['name']"
    _COMPLETED = "self.info['competitions'][0]['status']['type']['completed']"
    _CLOCK = "self.info['competitions'][0]['status']['displayClock']"
    _PERIOD = "self.info['competitions'][0]['status']['period']"
    _C0_ID = "self.info['competitions'][0]['competitors'][0]['id']"
    _C0_HOME_AWAY = "self.info['competitions'][0]['competitors'][0]['homeAway']"
    _C0_WINNER = "self.info['competitions'][0]['competitors'][0]['winner']"
    _CO_SCORE = "self.info['competitions'][0]['competitors'][0]['score']"
    _C1_ID = "self.info['competitions'][0]['competitors'][1]['id']"
    _C1_HOME_AWAY = "self.info['competitions'][0]['competitors'][1]['homeAway']"
    _C1_WINNER = "self.info['competitions'][0]['competitors'][1]['winner']"
    _C1_SCORE = "self.info['competitions'][0]['competitors'][1]['score']"

    _HEADLINE = "self.info['competitions'][0]['headlines'][0]['description']"
    _ATTENDANCE = "self.info['competitions'][0]['attendance']"
    _STADIUM = "self.info['competitions'][0]['venue']['fullName']"
    _CITY = "self.info['competitions'][0]['venue']['address']['city']"
    _STATE = "self.info['competitions'][0]['venue']['address']['state']"
    _SPREAD = "self.info['competitions'][0]['odds'][0]['details']"
    _OVER_UNDER = "self.info['competitions'][0]['odds'][0]['overUnder']"
    _WEATHER = "self.info['weather']['displayValue']"
    _HIGH_TEMP = "self.info['weather']['highTemperature']"

    def __init__(self, matchup_id, http=urllib3.PoolManager(), matchup_json=None):
        if matchup_json is None:
            # Use the ESPN URL to get the object
            url = self._SCOREBOARD_URL + matchup_id
            response = http.request('GET', url)
            self.info = json.loads(response.data.decode('utf-8'))
        else:
            # Use the supplied json - for testing purposes
            self.info = matchup_json

    def __str__(self):
        return eval(self._HEADLINE)

    def is_pregame(self):
        state = eval(self._GAME_STATE)
        if state == 'pre':
            return True
        else:
            return False

    def is_being_played(self):
        state = eval(self._GAME_STATE)
        if state != 'pre' and state != 'post':
            return True
        else:
            return False

    def is_complete(self):
        return eval(self._COMPLETED)

    def is_cancelled(self):
        state_name = eval(self._GAME_STATE_NAME)
        if state_name == 'STATUS_CANCELED':
            return True
        else:
            return False

    def is_postponed(self):
        state_name = eval(self._GAME_STATE_NAME)
        if state_name == 'STATUS_POSTPONED':
            return True
        else:
            return False

    def clock(self):
        return eval(self._CLOCK)

    def period(self):
        """Returns which quarter the game is in as an integer

        Returns 0 for a game not yet started, 1..4 for quarters 1-4, and 5 or more if a game goes into overtime.
        For example, the first overtime will return 5, the second will return 6, etc.

        :return: int between 0..N for the quarters of a football game
        """
        return int(eval(self._PERIOD))

    def quarter(self):
        """Returns which quarter the game is in as a string.

        :return: string either Pregame, 1st Quarter, 2nd Quarter, 3rd Quarter, 4th Quarter, or Overtime
        """
        period = int(eval(self._PERIOD))
        if period == 0:
            return 'Pregame'
        if period == 1:
            return '1st Quarter'
        elif period == 2:
            return '2nd Quarter'
        elif period == 3:
            return '3rd Quarter'
        elif period == 4:
            return '4th Quarter'
        else:
            return 'Overtime'

    def away_score(self):
        """ Returns the score for the away team

        :return: int that is the score for the team
        """
        if eval(self._C0_HOME_AWAY) == 'away':
            score_str = eval(self._CO_SCORE)
        else:
            score_str = eval(self._C1_SCORE)
        return int(score_str)

    def home_score(self):
        """ Returns the score for the home team

        :return: int that is the score for the team
        """
        if eval(self._C0_HOME_AWAY) == 'home':
            score_str = eval(self._CO_SCORE)
        else:
            score_str = eval(self._C1_SCORE)
        return int(score_str)

    def headline(self):
        return eval(self._HEADLINE)

    def attendance(self):
        return int(eval(self._ATTENDANCE))

    def stadium(self):
        return eval(self._STADIUM)

    def city(self):
        return eval(self._CITY)

    def state(self):
        return eval(self._STATE)

    def spread(self, max_length=15):
        """Returns the spread of the game if available or empty string if not available.

        :param int max_length: ensures the string length is no longer than max_length
        :return string representing the spread, for example 'FLA -6.5', or ''.
        """
        # Spread is not always available in the ESPN API so will get KeyErrors
        try:
            spread = eval(self._SPREAD)
        except KeyError:
            spread = ''

        spread = spread[0:max_length]
        return spread

    def over_under(self):
        """Returns the over_under of the game if available, None if not available.

        :return int representing over / under, or None.
        """
        # Over Under is not always available in the ESPN API so will get KeyErrors
        try:
            o_u = eval(self._OVER_UNDER)
            over_under = int(o_u)
        except KeyError:
            over_under = None
        return over_under

    def weather(self, max_length=25):
        """Returns the weather of the game if available, or empty string if not available.

        :param int max_length: ensures the string length is no longer than max_length
        :return: string representing the weather, for example 'Partly sunny', or ''
        """
        # Weather is not always available in the ESPN API so will get KeyErrors
        try:
            weather = eval(self._WEATHER)
        except KeyError:
            weather = ''

        weather = weather[0:max_length]
        return weather

    def high_temp(self):
        """Returns the high temperature the day of the game if available, None if not available.

        :return: int representing the high temperature or None.
        """
        # High Temp is not always available in the ESPN API so will get KeyErrors
        try:
            h_t = eval(self._HIGH_TEMP)
            high_temp = int(h_t)
        except KeyError:
            high_temp = None
        return high_temp


class LiveTeam(object):
    """Use this to get info about a team"""
    _TEAM_URL = 'http://site.api.espn.com/apis/site/v2/sports/football/college-football/teams/'

    # JSON paths
    _ID = "self.info['team']['id']"
    _ABBREV = "self.info['team']['abbreviation']"

    def __init__(self, team_code):
        http = urllib3.PoolManager()
        url = self._TEAM_URL + team_code
        response = http.request('GET', url)
        self.info = json.loads(response.data.decode('utf-8'))

    def id(self):
        try:
            ident = eval(self._ID)
        except KeyError:
            ident = 9999
        return ident

    def abbrev(self):
        try:
            abbrev = eval(self._ABBREV)
        except KeyError:
            abbrev = 'XXXX'
        return abbrev
