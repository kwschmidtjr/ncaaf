import json
import urllib3

from django.test import TestCase

from ..models import LiveMatchup, LiveWeek
from ...pool.models import Team, Matchup


class LiveMatchupTestUsingESPN(TestCase):
    matchup_id = '400934572'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.http = urllib3.PoolManager()

    def test_is_complete(self):
        matchup = LiveMatchup(self.matchup_id, self.http)
        complete = matchup.is_complete()
        self.assertEqual(complete, True)

    def test_home_score(self):
        matchup = LiveMatchup(self.matchup_id, self.http)
        score = matchup.home_score()
        self.assertEqual(score, 13)

    def test_away_score(self):
        matchup = LiveMatchup(self.matchup_id, self.http)
        score = matchup.away_score()
        self.assertEqual(score, 14)

    def test_clock(self):
        matchup = LiveMatchup(self.matchup_id, self.http)
        clock = matchup.clock()
        self.assertEqual(clock, '0:00')

    def test_period(self):
        matchup = LiveMatchup(self.matchup_id, self.http)
        period = matchup.period()
        self.assertEqual(period, 4)

    def test_headline(self):
        matchup = LiveMatchup(self.matchup_id, self.http)
        headline = matchup.headline()
        actual_headline = "Bennett Moehring narrowly missed a 48-yard field goal in a swirling snow on the final " + \
                          "play and Army held off Navy 14-13 Saturday to win its first Commander-in-Chief's Trophy since 1996."
        self.assertEqual(headline, actual_headline)

    def test_other(self):
        # Yes this is combining multiple unit tests, but this is just basic stuff to see if ESPN changed their
        # JSON model.  I don't mind digging into the specific items here to determine which one broke if this
        # overall test fails.
        matchup = LiveMatchup(self.matchup_id, self.http)
        attendance = matchup.attendance()
        stadium = matchup.stadium()
        city = matchup.city()
        state = matchup.state()

        self.assertEqual(attendance, 68625)
        self.assertEqual(stadium, 'Lincoln Financial Field')
        self.assertEqual(city, 'Philadelphia')
        self.assertEqual(state, 'PA')


class LiveMatchupTestPreGame(TestCase):
    # JSON_FILE = 'live_matchup_pregame.json'
    JSON_FILE = 'ncaaf/apps/stats/tests/live_matchup_pregame.json'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Loading the json during __init__ instead of SetUp for efficiency
        # The json never changes so the extra overhead not needed
        with open(self.JSON_FILE, encoding='utf-8-sig') as json_file:
            self.data = json.load(json_file)
        self.http = urllib3.PoolManager()

    def test_is_pregame(self):
        matchup = LiveMatchup(None, self.http, self.data)
        complete = matchup.is_pregame()
        self.assertEqual(complete, True)

    def test_is_complete(self):
        matchup = LiveMatchup(None, self.http, self.data)
        complete = matchup.is_complete()
        self.assertEqual(complete, False)

    def test_quarter(self):
        matchup = LiveMatchup(None, self.http, self.data)
        quarter = matchup.quarter()
        self.assertEqual(quarter, 'Pregame')

    def test_other(self):
        # These tests are basic and already tested elsewhere, but this info is key for integration tests
        # so this is just to ensure those tests run successfully
        matchup = LiveMatchup(None, self.http, self.data)
        score = matchup.home_score()
        clock = matchup.clock()
        period = matchup.period()
        attendance = matchup.attendance()
        self.assertEqual(score, 0)
        self.assertEqual(clock, '0:00')
        self.assertEqual(period, 0)
        self.assertEqual(attendance, 0)


class LiveMatchupTestInGame(TestCase):
    JSON_FILE = 'ncaaf/apps/stats/tests/live_matchup_ingame.json'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Loading the json during __init__ instead of SetUp for efficiency
        # The json never changes so the extra overhead not needed
        import os
        wd = os.getcwd()
        with open(self.JSON_FILE, encoding='utf-8-sig') as json_file:
            self.data = json.load(json_file)
        self.http = urllib3.PoolManager()

    def test_is_complete(self):
        matchup = LiveMatchup(None, self.http, self.data)
        complete = matchup.is_complete()
        self.assertEqual(complete, False)

    def test_is_being_played(self):
        matchup = LiveMatchup(None, self.http, self.data)
        complete = matchup.is_being_played()
        self.assertEqual(complete, True)

    def test_quarter(self):
        matchup = LiveMatchup(None, self.http, self.data)
        quarter = matchup.quarter()
        self.assertEqual(quarter, '3rd Quarter')

    def test_other(self):
        # These tests are basic and already tested elsewhere, but this info is key for integration tests
        # so this is just to ensure those tests run successfully
        matchup = LiveMatchup(None, self.http, self.data)
        away_score = matchup.away_score()
        clock = matchup.clock()
        period = matchup.period()
        self.assertEqual(away_score, 31)
        self.assertEqual(clock, '12:30')
        self.assertEqual(period, 3)


class LiveMatchupTestPostGame(TestCase):
    JSON_FILE = 'ncaaf/apps/stats/tests/live_matchup_postgame.json'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Loading the json during __init__ instead of SetUp for efficiency
        # The json never changes so the extra overhead not needed
        with open(self.JSON_FILE, encoding='utf-8-sig') as json_file:
            self.data = json.load(json_file)
        self.http = urllib3.PoolManager()

    def test_is_complete(self):
        matchup = LiveMatchup(None, self.http, self.data)
        complete = matchup.is_complete()
        self.assertEqual(complete, True)

    def test_is_being_played(self):
        matchup = LiveMatchup(None, self.http, self.data)
        complete = matchup.is_being_played()
        self.assertEqual(complete, False)

    def test_quarter(self):
        matchup = LiveMatchup(None, self.http, self.data)
        quarter = matchup.quarter()
        self.assertEqual(quarter, '4th Quarter')

    def test_other(self):
        # These tests are basic and already tested elsewhere, but this info is key for integration tests
        # so this is just to ensure those tests run successfully
        matchup = LiveMatchup(None, self.http, self.data)
        away_score = matchup.away_score()
        home_score = matchup.home_score()
        clock = matchup.clock()
        period = matchup.period()
        attendance = matchup.attendance()
        self.assertEqual(away_score, 38)
        self.assertEqual(home_score, 3)
        self.assertEqual(clock, '0:00')
        self.assertEqual(period, 4)
        self.assertEqual(attendance, 80580)


class LiveMatchupTestOddsWeather(TestCase):
    JSON_FILE_WITH = 'ncaaf/apps/stats/tests/live_matchup_with_odds_weather.json'
    JSON_FILE_WITHOUT = 'ncaaf/apps/stats/tests/live_matchup_pregame.json'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.http = urllib3.PoolManager()
        self.data_with = None
        self.data_without = None

    def _load_with(self):
        with open(self.JSON_FILE_WITH, encoding='utf-8-sig') as json_file:
            self.data_with = json.load(json_file)

    def _load_without(self):
        with open(self.JSON_FILE_WITHOUT, encoding='utf-8-sig') as json_file:
            self.data_without = json.load(json_file)

    def test_spread(self):
        self._load_with()
        matchup = LiveMatchup(None, self.http, self.data_with)
        spread = matchup.spread()
        self.assertEqual(spread, 'FLA -6.5')

    def test_spread_not_there(self):
        self._load_without()
        matchup = LiveMatchup(None, self.http, self.data_without)
        spread = matchup.spread()
        self.assertEqual(spread, '')

    def test_over_under(self):
        self._load_with()
        matchup = LiveMatchup(None, self.http, self.data_with)
        over_under = matchup.over_under()
        self.assertEqual(over_under, 57)

    def test_over_under_not_there(self):
        self._load_without()
        matchup = LiveMatchup(None, self.http, self.data_without)
        over_under = matchup.over_under()
        self.assertEqual(over_under, None)

    def test_weather(self):
        self._load_with()
        matchup = LiveMatchup(None, self.http, self.data_with)
        weather = matchup.weather()
        self.assertEqual(weather, 'Partly sunny')

    def test_weather_not_there(self):
        self._load_without()
        matchup = LiveMatchup(None, self.http, self.data_without)
        weather = matchup.weather()
        self.assertEqual(weather, '')

    def test_high_temp(self):
        self._load_with()
        matchup = LiveMatchup(None, self.http, self.data_with)
        high_temp = matchup.high_temp()
        self.assertEqual(high_temp, 91)

    def test_high_temp_not_there(self):
        self._load_without()
        matchup = LiveMatchup(None, self.http, self.data_without)
        high_temp = matchup.high_temp()
        self.assertEqual(high_temp, None)


class LiveWeekTest(TestCase):
    JSON_FILE = 'ncaaf/apps/stats/tests/live_week.json'
    TEAM_FILE = 'ncaaf/apps/stats/tests/teams.json'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Loading the json during __init__ instead of SetUp for efficiency
        # The json never changes so the extra overhead not needed
        with open(self.JSON_FILE, encoding='utf-8-sig') as json_file:
            self.data = json.load(json_file)
        self.http = urllib3.PoolManager()

    def _load_teams(self):

        with open(self.TEAM_FILE, encoding='utf-8') as data_file:
            json_data = json.loads(data_file.read())

            for team in json_data:
                Team.objects.create(
                    id=team['id'],
                    code=team['code'],
                    name=team['name'],
                    mascot=team['mascot'],
                    conference=team['conference'],
                    division=team['division'],
                    espn_id=team['espn_id'],
                )

    def test_load_matchups(self):
        self._load_teams()
        live_week = LiveWeek(1, self.http, self.data)
        matchups_added, matchups_skipped = live_week.load_matchups()
        self.assertEqual(16, matchups_added)

    def test_update_odds(self):
        self._load_teams()
        live_week = LiveWeek(1, self.http, self.data)
        live_week.load_matchups()
        live_week.update_odds()
        matchup = Matchup.objects.filter(espn_id=401246275)
        self.assertEqual(matchup.first().current_spread, 'BYU -23.5')

