from django.apps import AppConfig


class StatsConfig(AppConfig):
    name = 'ncaaf.apps.stats'
