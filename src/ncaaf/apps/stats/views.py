from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone

from openpyxl.writer.excel import save_virtual_workbook

from .models import LiveTeam, LiveWeek
from ..pool.models import Matchup, Pick, WeeklyStats, Config
from ..pool.utils import ExcelUtility, BackupRestore
from ..pool.tasks import notify_picks


def batch_runner(request):
    template_name = 'stats/batch_runner.html'
    title = 'CFP - Batch Runner'

    job = request.GET.get('job')

    if job == 'load_matchups':
        live_week = LiveWeek(Config.get_week())
        matchups_added, matchups_skipped = live_week.load_matchups()
        result = '<p>{} matchups added.  These were skipped:</p><ul>'.format(matchups_added)
        for skipped in matchups_skipped:
            result += '<li>{}'.format(skipped)
        result += '</ul>'
    elif job == 'load_matchup':
        espn_id = request.GET.get('id')
        if espn_id is None or espn_id == '':
            result = ''' 
                <p>Add the ESPN ID to the URL</p>
                <div class="alert alert-primary" role="alert">
                <p><b>Don't forget to change the week!</b></p>
                </div>
                '''
        else:
            live_week = LiveWeek(Config.get_week())
            matchup_added, matchup_skipped = live_week.load_matchup(espn_id)
            result = '<p>{}</p>'.format(matchup_skipped)
    elif job == 'update_matchups':
        live_week = LiveWeek(Config.get_week())
        complete, pregame, in_game, post_game, postponed = live_week.update_matchups(Config.get_year(), Config.get_week())
        result = '<p>Matchups updated:</p>' + \
                 '<ul><li>{} complete</li>'.format(complete) + \
                 '<li>{} pregame</li>'.format(pregame) + \
                 '<li>{} in game</li>'.format(in_game) + \
                 '<li>{} post game</li>'.format(post_game) + \
                 '<li>{} postponed</li></ul>'.format(postponed)
    elif job == 'flip_matchups':
        flipped = Matchup.move_to_locked(timezone.now(), Config.get_year(), Config.get_week())
        result = '<p>{} matchups flipped to locked.</p>'.format(flipped)
    elif job == 'notify_picks':
        picks = notify_picks()
        result = '<p>Sent {} notifications</p>'.format(picks)
    elif job == 'calc_stats':
        stats = WeeklyStats.calc_weekly_stats(Config.get_year(), Config.get_week())
        result = '<p>Calculated stats for {} users</p>'.format(stats)
    elif job == 'export':
        workbook = ExcelUtility.create_workbook()
        response = HttpResponse(content=save_virtual_workbook(workbook), content_type='application/ms-excel')
        response['Content-Disposition'] = \
            'attachment; filename=CollegeFBPool-{}-Week-{}.xlsx'.format(Config.get_year(), Config.get_week())
        return response
    # elif job == 'backup':
    #     backup = BackupRestore()
    #     backup.backup()
    #     result = '<p>Done</p>'
    # elif job == 'restore':
    #     restore = BackupRestore()
    #     result = restore.restore()
    #     result = '<p>{}</p>'.format(result)
    elif job == 'make_picks':
        Pick.make_empty_picks(Config.get_year(), Config.get_week())
        result = '<p>New pick objects created where needed'
    else:
        result = 'no valid batch run'

    return render(request, template_name, {'result': result, 'title': title})
