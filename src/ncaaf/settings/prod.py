from ._base import *

# import sentry_sdk
# from sentry_sdk.integrations.django import DjangoIntegration

# sentry_sdk.init(
#      dsn="https://76d31b4c773d46e39991335c2a4ef3da@o429994.ingest.sentry.io/5377753",
#      integrations=[DjangoIntegration()],
#      # If you wish to associate users to errors (assuming you are using
#      # django.contrib.auth) you may enable sending PII data.
#      send_default_pii=True
# )

DEBUG = True

# EMAIL_HOST = 'smtp.gmail.com'
# EMAIL_PORT = 587
# EMAIL_HOST_USER = ''
# EMAIL_HOST_PASSWORD = ''
# EMAIL_USE_TLS = True
# DEFAULT_FROM_EMAIL = 'CollegeFBPool Admin <pool-admin@collegefbpool.com'

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "ncaaf",
        "USER": "postgres",
        "PASSWORD": "password",  # yes, this needs to be changed...
        "HOST": "db",
        "PORT": 5432,
    }
}
