from ._base import *

DEBUG = True
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        # 'NAME': os.path.join(BASE_DIR, '../build/db.sqlite3'),
        'NAME': '/Users/kwschmidtjr/Dev/ncaaf/build/db.sqlite3',
    }
}
