import os
from celery import Celery

from django.conf import settings

# TODO: change this to dynamically get settings (prod vs dev)
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ncaaf.settings.prod')

app = Celery('ncaaf')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


# @app.task(bind=True)
# def debug_task(self):
#    print('Request: {0!r}'.format(self.request))
