MANAGE_PY := cd src && python manage.py
COMPOSE_LOCAL := docker-compose -f docker-compose.local.yml

#---------------------------------------------------------------------------
# Running Django developer commands
#---------------------------------------------------------------------------
ensure_build_dir:
	rm -rf ./build/ && mkdir build

migrate: ensure_build_dir
	$(MANAGE_PY) migrate

run: migrate
	$(MANAGE_PY) runserver

shell:
	$(MANAGE_PY) shell

test: ensure_build_dir
	$(MANAGE_PY) test


#---------------------------------------------------------------------------
# Run / Manage app cluster on local dev machine
#---------------------------------------------------------------------------
cluster-ps:
	$(COMPOSE_LOCAL) ps -a

# This may need to be updated with commands to delete all the Docker volumes
# to truly delete the entire database so all the DB state will be gone on the
# next `cluster-run`
cluster-clean:
	$(COMPOSE_LOCAL) rm -sf

cluster-run:
	$(COMPOSE_LOCAL) up --build -d
	$(COMPOSE_LOCAL) exec -T web python manage.py makemigrations
	$(COMPOSE_LOCAL) exec -T web python manage.py migrate
	$(MAKE) cluster-ps

cluster-up:
	$(COMPOSE_LOCAL) up --build

cluster-down:
	$(COMPOSE_LOCAL) down


#---------------------------------------------------------------------------
# Container Commands (mostly used in CI environment)
#---------------------------------------------------------------------------
# This will tag the images built by `docker build` with the version number
# equal to CI_COMMIT_SHA during a Gitlab build pipeline execution.
# Otherwise, it will just tag them with the version `local`.
#
# The latter scenario can be used down-the-road to manually build and tag
# images on a developer's machine if one wanted to do something like
# extracting the image versions in the Compose files into env variables.
ifdef CI_COMMIT_SHA
VERSION := $(CI_COMMIT_SHA)
else
VERSION := local
endif

APP_CONTAINER := registry.gitlab.com/kwschmidtjr/ncaaf/app:${VERSION}
NGINX_CONTAINER := registry.gitlab.com/kwschmidtjr/ncaaf/nginx:${VERSION}

build-containers:
	docker build -t $(APP_CONTAINER) -f Dockerfile .
	docker build -t $(NGINX_CONTAINER) -f Dockerfile.nginx .

push-containers:
	docker push $(APP_CONTAINER)
	docker push $(NGINX_CONTAINER)
