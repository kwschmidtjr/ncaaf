#!/bin/bash

#-------------------------------------------------------------------------------
# This script provides some shell commands that were used to get the initial
# issuance of the SSL certificates for the 2x collegefbpool domains.
#
# It also provides a command to verify if a certbot renewal via HTTP validation
# will work.
#-------------------------------------------------------------------------------
EMAIL="kwschmidtjr@gmail.com"
RSA_KEY_SIZE=4096
CERT_NAME=collegefbpool.com

# This directory is mounted from prod VM into the nginx container
WEBROOT_PATH=/var/www/certbot

# This is a command to issue Let's Encrypt SSL Staging certs for the domains
issue_cert_staging() {
  sudo certbot certonly \
   --staging \
    --standalone \
    --email "$EMAIL" \
    --rsa-key-size "$RSA_KEY_SIZE" \
    --agree-tos \
    --force-renewal \
    --non-interactive \
    --cert-name "$CERT_NAME" \
    -d collegefbpool.com \
    -d www.collegefbpool.com
}

# This is a command to issue Let's Encrypt SSL Prod certs for the domains
issue_cert() {
  sudo certbot certonly \
    --standalone \
    --email "$EMAIL" \
    --rsa-key-size "$RSA_KEY_SIZE" \
    --agree-tos \
    --force-renewal \
    --non-interactive \
    --cert-name "$CERT_NAME" \
    -d collegefbpool.com \
    -d www.collegefbpool.com

}

# This command is run after the initial 'standalone' certificate issuance.  It
# sets up automated renewals on the host to use the 'webroot' method which will
# work with the actively running prod nginx container.
setup_renewal() {
  sudo certbot reconfigure \
    --cert-name "$CERT_NAME" \
    --webroot \
    --webroot-path "$WEBROOT_PATH"
}

# This will verify if renewal has proper access to HTTP port that is required on renewal
renew_cert_dry_run() {
  sudo certbot renew --dry-run
}

# Uncomment whichever command you wish to run
#issue_cert_staging
#issue_cert
#setup_renewal
renew_cert_dry_run
