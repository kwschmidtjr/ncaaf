#!/bin/bash
#-------------------------------------------------------------------------------
# This script describes the steps run in 8/2023 to install the most recent
# version of Gitlab's self-manager Runner for Ubuntu 20.04.  The Runner needed
# to be updated because the current Runner's version was a few major versions behind
# gitlab.com's current version.
#
# Instructions taken from:
#  - https://certbot.eff.org/instructions?ws=nginx&os=ubuntufocal
#-------------------------------------------------------------------------------
sudo apt-get remove certbot
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo systemctl status snap.certbot.renew.service
sudo systemctl status snap.certbot.renew.timer
